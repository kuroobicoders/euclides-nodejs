// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const router = express.Router();

// Get our API routes
const resultRoutes = require('./routes/result.routes');
const raceRoutes = require('./routes/race.routes');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// CORS
app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

   next();
});

// Set api routes
app.use('/api', resultRoutes);
app.use('/api', raceRoutes);
app.all('/*', function(req, res) {
    res.sendFile(path.join(__dirname, 'dist') + '/index.html');
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));

const mongojs = require('mongojs');
const yaml = require ('yamljs');
const ObjectId = require('mongodb').ObjectID;
const euclidesConfig = yaml.load ('euclides-api.yml');
const collection = euclidesConfig.database.schema;

const db = mongojs(euclidesConfig.database.url);

const notFederated = euclidesConfig.result.notFederated.split(",");
const notTeam = euclidesConfig.result.notTeam.split(",");
const maleGroup = euclidesConfig.result.maleGroup;
const femaleGroup = euclidesConfig.result.femaleGroup;

const ALL_GROUPS = "global";
const ALL_CATEGORIES = "all-categories";
const ALL_TEAMS = "all-teams";
const ALL_ATHLETES = "all-athletes";
const ALL_SPLITS = -1;

const TYPE = "result";
const MALE_GROUP = "male";
const FEMALE_GROUP = "female";
const COMPLETE_RACE_STATUS = "F";
const DEFAULT_ATHLETES_FOR_TEAM_RANKING = 3;

function health (request, response) {
    response.status(200).send({"status":"UP"})
}

function getResult (request, response) {
    let resultId = request.params.resultId;

    db.collection(collection)
        .find(ObjectId(resultId)).limit(1).next(
        (error, result) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                if (result != null) {
                    response.status(200).json(result);
                } else {
                    response.status(404).send({ error: "No results for id: " + resultId });
                }
            }
        }
    )
}

function countByTeam (request, response) {
    let editionId = request.params.editionId;

    db.collection(collection).aggregate (
            {$match: { "edition": editionId, "type": TYPE, "state": COMPLETE_RACE_STATUS, "team.url":{$nin: notTeam.concat(notFederated)}}},
            {$group: {
                "_id": "$team",
                "count" : { $sum : 1}
            }},
            {$sort: {"count": -1, "_id.url": 1}},
            {$project: {
                "_id": 0,
                "team": "$_id",
                "athletes": "$count"
            }},
        (error, data) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                if (data != null && data.length > 0) {
                    response.status(200).json(data);
                } else {
                    response.status(404).send({ error: "No results data for edition: " + editionId });
                }
            }
        } 
    )
}

function getResultsTeamsAndCategories (request, response) {
    let editionId = request.params.editionId;

    db.collection(collection).aggregate (
            {$match: { "edition": editionId, "type": TYPE}},
            {$group: {
                "_id": null,
                "categories": { "$addToSet": "$category" },
                "teams": { "$addToSet": "$team" },
                "groups": { "$addToSet": "$group" },
                "notTeam": {$sum: { $cond: { if: {$in: [ "$team.url", notTeam ]}, then: 1, else: 0 } }},
                "notFederated": {$sum: { $cond: { if: {$in: [ "$team.url", notFederated ]}, then: 1, else: 0 } }},
                "men": {$sum: { $cond: { if: {$eq: [ "$group", maleGroup ]}, then: 1, else: 0 } }},
                "women": {$sum: { $cond: { if: {$eq: [ "$group", femaleGroup ]}, then: 1, else: 0 } }}
            }},
        (error, data) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                if (data != null && data.length > 0) {
                    response.status(200).json(data[0]);
                } else {
                    response.status(404).send({ error: "No results data for edition: " + editionId });
                }
            }
        } 
    )
}

/* 
  Get ranking for a given edition
    @param: editionId (edition)
    @query-params: group, category, split, team, athletes.
*/
function getRankingByEdition (request, response) {
    let editionId = request.params.editionId;
    let group = request.query.group;
    let category = request.query.category;
    let team = request.query.team;
    let split = request.query.split;
    let athletes = request.query.athletes;
    let sort = {};
    let find = {};

    sort["state"] = 1;
    find["type"] = TYPE;
    find["edition"] = editionId;

    if (group != undefined && group != ALL_GROUPS) {
        find["group"] = group;
    }

    if (category != undefined && category != ALL_CATEGORIES) {
        find["category"] = category;
    }

    if (team != undefined && team != ALL_TEAMS) {
        find["team.url"] = team;
    }

    if (athletes != undefined && athletes != ALL_ATHLETES) {
        let athletesAsObjectIds = [];
        athletes = athletes.split(",");
        for (i=0; i<athletes.length; i++) {
            athletesAsObjectIds.push(ObjectId(athletes[i]));
        }
        find["_id"] = {$in: athletesAsObjectIds};
    }
    
    if (split != undefined && split != ALL_SPLITS) {
        find["splits." + split] = {$ne:0}
        sort["splits." + split] = 1;
    } else {
        sort["total"] = 1;
    }

   db.collection(collection)
        .find(find)
        .sort(sort, 
    (error, ranking)  => {
        if (error) {
            response.status(500).send(error);
        } else {
            if (ranking != null && ranking.length > 0) {
                response.status(200).json(ranking);
            } else {
                response.status(404).send({ error: "No results data for edition: " + editionId });
            }
        }
    });
}

function getAthleteRanking (request, response) {
    let resultId = request.params.resultId;

    db.collection(collection)
        .find(ObjectId(resultId)).limit(1).next(

        (error, result) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                if (result != null) {
                    
                    partial = new Array();
                    global = new Array();
                    
                    aggregations = {};
                    aggregations["_id"] = "rank";

                    result.splits.forEach (function(split, i) {
                        let currentSplit = "split" + i;
                        let currentScore = "score" + i;

                        aggregations[currentSplit] = {$sum: { $cond: { if: {$and : [ { $lte: [ {$arrayElemAt: [ "$splits", i ]}, split] }, { $gt: [ {$arrayElemAt: [ "$splits", i ]}, 0] }] }, then: 1, else: 0 } }};
                        partial.push("$" + currentSplit);
                        
                        aggregations[currentScore] = {$sum: { $cond: { if: {$and : [ { $lte: [ {$arrayElemAt: [ "$scores", i ]}, result.scores[i]] }, { $gt: [ {$arrayElemAt: [ "$scores", i ]}, 0] }] }, then: 1, else: 0 } }};
                        global.push("$" + currentScore);
                    });

                    aggregations["total"] = {$sum: { $cond: { if: {$and : [ { $lte: [ "$total", result.total] }, { $gt: [ "$total", 0] }] }, then: 1, else: 0 } }};
                    partial.push ("$total");
                    global.push ("$total");

                    aggregations["athletes"] = {$sum: 1}

                    db.collection(collection).aggregate (
                        {
                            $match: {"type": TYPE, "edition":result.edition, "state":COMPLETE_RACE_STATUS, "group":result.group}
                        },
                        {
                            $group: aggregations
                        },
                        {
                            $project: {
                                _id: 0,
                                "partial": partial,
                                "global": global,
                                "total": "$athletes"
                            }
                        },
                        (error, rank) => {
                            if (error) {
                                response.status(500).send(error);
                            } else {
                                if (rank != null && rank.length > 0) {
                                    response.status(200).json(rank[0]);
                                }
                            }
                        }
                    )
                } else {
                    response.status(404).send({ error: "No results for id: " + resultId });
                }
            }
        }
    )
}

/* Team ranking
    @param: editionId (edition)
    @query-params: group, numberOfAtheletes
*/
function getTeamRankingByEdition (request, response) {
    let edition = request.params.editionId;
    let numberOfAthletes = request.query.numberOfAthletes;
    let group = request.query.group;
    let match = {};

    match["type"] = TYPE;
    match["edition"] =  edition;
    match["state"] = COMPLETE_RACE_STATUS;

    if (numberOfAthletes == undefined) {
        numberOfAthletes = DEFAULT_ATHLETES_FOR_TEAM_RANKING;
    } else {
        numberOfAthletes = Number(numberOfAthletes);
    }

    if (group == undefined && group != MALE_GROUP && group != FEMALE_GROUP) {
        group = MALE_GROUP;
    }

    match["group"] = group;
    match["team.url"] = {$nin: notFederated.concat(notTeam)};

    db.collection(collection).aggregate(
        {
            $match: match
        },
        {
            $sort: ({"team.url": 1, "total": 1})
        },
        {
            $group: ({
                _id: "$team",
                number: {$sum: 1},
                results: { $push : {"name": "$name", "splits":"$splits", "total":"$total", "_id": "$_id"} }
            })
        },
        {
            $match: ({number: {$gte: numberOfAthletes}})
        },
        { 
            $project: { 
                results: { "$slice": [ "$results", numberOfAthletes ] },
            }
        },
        {
            "$unwind": { path: "$results" }
        },
        {
            $group: ({
                _id: "$_id",
                total: {$sum: "$results.total"},
                results: { $push : {"name": "$results.name", "splits":"$results.splits", "total":"$results.total", "_id": "$results._id"} }
            })
        },
        {
            $sort: {"total": 1}
        },
        {
            $project: {
                _id: 0,
                team: "$_id",
                total: "$total",
                results: "$results"
            }
        },

        (error, ranking) => {
            if (error) {
                response.status(500).send(error);
            } else {
                if (ranking != null && ranking.length > 0) {
                    response.status(200).json(ranking);
                } else {
                    response.status(404).send({ error: "No results for edition: " + edition });
                }
            }
        }
    )
}

/* Get history for a given result (same athlete in races of different years) */
function getResultsHistory (request, response) {
    let resultId = request.params.resultId;

    db.collection(collection)
        .find(ObjectId(resultId)).limit(1).next(

        (error, result) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                if (result != null) {

                    let regex = "";
                    result.name_normalized.split (" ").forEach (name => {
                        regex += "(?=.*" + name.toUpperCase() + ")"
                    })

                    db.collection(collection).find({
                        "type":TYPE, 
                        "race":result.race, 
                        "name_normalized":{$regex: regex}
                    }).sort({"year": 1},
                (error, history) => {
                    if (error) {
                        response.status(500).send({ error: error });
                    } else {
                        response.status(200).json(history);
                    }
                });
                } else {
                    response.status(404).send({ error: "No results for id: " + resultId });
                }
            }
        }
    )
}

/* 
  Get average for a given edition
    @param: editionId (edition)
    @query-params: group, category, team, top.
*/
function getResultsAverageByEdition (request, response) {
    let edition = request.params.editionId;
    let group = request.query.group;
    let category = request.query.category;
    let team = request.query.team;
    let top = parseInt(request.query.top);

    let limit = Number.MAX_SAFE_INTEGER;

    let match = {};
    match["type"]= TYPE;
    match["edition"]= edition;
    match["state"]= COMPLETE_RACE_STATUS;

    if (group != undefined && group != ALL_GROUPS) {
        match["group"] = group;
    }

    if (category != undefined && category != ALL_CATEGORIES) {
        match["category"] = category;
    }

    if (team != undefined && team != ALL_TEAMS) {
        match["team.url"] = team;
    }

    if (top != undefined && top >= 0) {
        limit = top;
    }

    db.collection(collection).aggregate(
        {
            $match: match
        },
        {
            $sort: ({"total": 1})
        },
        {
            $limit: limit
        }, 
        {
            $unwind : { path: "$splits", includeArrayIndex: "index" }
        },
        {
            $group: ({
                _id: {"edition": "$edition", index: "$index"},
                
                average: {$avg: "$splits"},
                total: {$avg: "$total"}
            })
        },
        {
            $sort: ({"_id.index": 1})
        },
        {
            $group: ({
                _id: "$_id.edition",
                splits: {$push: "$average"},
                total: {$avg: "$total"}
            })
        },
        {
            $project: ({
                _id: 0,
                splits: "$splits",
                total: "$total"
            })
        },
        (error, result) => {
            if (error) {
                response.status(500).send(error);
            } else {
                if (result != null && result.length > 0) {
                    response.status(200).json(result[0]);
                } else {
                    response.status(404).send({ error: "No results for edition: " + edition });
                }
            }
        }
    )
}

function getAtheletesByEdition (request, response) {
    let editionId = request.params.editionId;
    let team = request.query.team;
    
    let find = {};
    find["type"]= TYPE;
    find["edition"]= editionId;

    if (team != undefined && team != ALL_TEAMS) {
        find["team.url"] = team;
    }

    db.collection(collection)
        .find(find,{"name": 1, "state": 1})
        .sort({"name_normalized": 1}, 
    (error, athletes)  => {
        if (error) {
            response.status(500).send(error);
        } else {
            if (athletes != null && athletes.length > 0) {
                response.status(200).json(athletes);
            } else {
                response.status(404).send({ error: "No results data for edition: " + editionId });
            }
        }
    });
}

module.exports = {
    health,
    getRankingByEdition, getTeamRankingByEdition, getResult,
    getResultsTeamsAndCategories, getResultsAverageByEdition,
    getResultsHistory,
    getAthleteRanking, getAtheletesByEdition,
    countByTeam
};
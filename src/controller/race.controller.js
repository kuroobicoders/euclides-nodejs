const mongojs = require('mongojs');
const yaml = require ('yamljs');
const euclidesConfig = yaml.load ('euclides-api.yml');
const collection = euclidesConfig.database.schema;

const db = mongojs(euclidesConfig.database.url);

const TYPE = "race";

function health (request, response) {
    response.status(200).send({"status":"UP"})
}

/* Get All races */
function getRaces (request, response) {
    db.collection(collection)
            .find({"type":TYPE})
            .sort({"@timestamp": -1}, 
        (error, races) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                response.status(200).json(races);
            }
        });
}

/* Get a single races by its editionId */
function getRace (request, response) {
    let editionId = request.params.editionId;

    db.collection(collection)
            .find({"type":TYPE, "edition":editionId})
            .limit(1)
            .next (
        (error, race) => {
            if (error) {
                response.status(500).send({ error: error });
            } else {
                if (race != null) {
                    response.status(200).json(race);
                } else {
                    response.status(404).send({ error: "No race for edition: " + editionId });
                }
            }
        });
}

module.exports = {
    health,
    getRaces, getRace
};
const express = require('express');
const router = express.Router();

const raceController = require ("../controller/race.controller");

/* Get All  */
router.get('/races', raceController.getRaces);

/* Get a single races by its editionId */
router.get('/race/:editionId', raceController.getRace);

module.exports = router;
const express = require('express');
const router = express.Router();

const resultController = require ("../controller/result.controller");

/* Get api status. */
router.get('/health', resultController.health);

/* Get result by id */
router.get('/result/:resultId', resultController.getResult);

/* 
  Get ranking for a given edition
    @param: editionId (edition)
    @query-params: group, category, team and split.

    Default values:
     * group: global
     * category: all-categories
     * team: all-teams
     * athletes: all-athletes
     * split: -1
*/
router.get('/ranking/:editionId', resultController.getRankingByEdition);

/* Get team and categories a given edition by group */
router.get('/results/teams-and-categories/:editionId', resultController.getResultsTeamsAndCategories);

/* 
  Get average for a given edition
    @param: editionId (edition)
    @query-params: group, category, team and top.

    Default values:
     * group: global
     * category: all-categories
     * team: all-teams
     * top: -1
*/
router.get('/results/average/:editionId', resultController.getResultsAverageByEdition);

/* Count results for a given edition by team */
router.get('/results/count-by-team/:editionId', resultController.countByTeam);

/* History of a given athleye in a race */
router.get('/results/history/:resultId', resultController.getResultsHistory);

/* Ranking for a given result (athlete) */
router.get('/athletes/ranking/:resultId', resultController.getAthleteRanking);

/* Atheltes for a given edition (optional: team) */
router.get('/athletes/:editionId', resultController.getAtheletesByEdition);

/* Team ranking
    @param: editionId (edition)
    @query-params: group, numberOfAtheletes, ignoredTeams (team.url)

    Default values:
      * group: male
      * numberOfAtheltes: 3
*/
router.get('/team/ranking/:editionId', resultController.getTeamRankingByEdition);

module.exports = router;
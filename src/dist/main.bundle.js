webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet> </router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(translate) {
        this.title = 'app works!';
        translate.setDefaultLang('es');
        translate.use('es');
    }
    ;
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__routing_app_route_module__ = __webpack_require__("../../../../../src/app/routing/app-route.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_moment__ = __webpack_require__("../../../../angular2-moment/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_select__ = __webpack_require__("../../../../ng2-select/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_modules_translate_shared_module_translate_shared_module_module__ = __webpack_require__("../../../../../src/app/shared/modules/translate-shared-module/translate-shared-module.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__race_selector_race_selector_component__ = __webpack_require__("../../../../../src/app/race-selector/race-selector.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__dashboards_dashboard_module__ = __webpack_require__("../../../../../src/app/dashboards/dashboard.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_10__race_selector_race_selector_component__["a" /* RaceSelectorComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_6_angular2_moment__["MomentModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_4__routing_app_route_module__["a" /* AppRouteModule */],
                __WEBPACK_IMPORTED_MODULE_7_ng2_select__["SelectModule"],
                __WEBPACK_IMPORTED_MODULE_11__dashboards_dashboard_module__["a" /* DashboardModule */],
                __WEBPACK_IMPORTED_MODULE_8__shared_modules_translate_shared_module_translate_shared_module_module__["a" /* TranslateSharedModule */]
            ],
            providers: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* RouterModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n  <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n  <span class=\"sr-only\">Loading...</span>\n</div>\n\n<ng-container *ngIf=\"!isLoading\">\n  <ng-container *ngIf=\"!result && !error\">\n    <div id=\"athlete-selector\" class=\"flex-row align-items-center euclides-background-color-2\">\n      <div class=\"container\">\n        <div class=\"row justify-content-center\">\n          <div class=\"col-lg-8\">\n            <div class=\"card p-4 euclides-background-color-1 euclides-border-color-6\">\n              <div class=\"card-body\">\n                <p>{{ 'dashboard.select-athlete-title' | translate }}</p>\n                <ng-select\n                  [items]=\"athletes\"\n                  (selected)=\"selectAthlete($event)\"\n                  placeholder= \"{{ 'dashboard.select-athlete' | translate }}\"\n                >\n                </ng-select>\n\n                <p class=\"text-danger pull-right clearfix\" *ngIf=\"noAthleteSelectedError\">\n                  {{ 'dashboard.errors.no-athlete-selected' | translate }}\n                </p>\n\n                <p class=\"pull-right clearfix\">\n                  <button class=\"btn btn-info actionable\" (click)=\"goToAthlete()\">\n                    {{ 'dashboard.show-stats' | translate }}\n                  </button>\n                </p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </ng-container>\n\n  <ng-container *ngIf=\"!error && result\">\n    <div class=\"container-fluid pt-4\">\n      <h2> \n        {{ result.name }}\n        <a class=\"badge badge-pill badge-info p-1\" routerLink=\"..\">\n          {{ 'dashboard.change-it' | translate }} \n          <i class=\"fa fa-pencil-square\" aria-hidden=\"true\"></i>\n        </a>\n      </h2>\n\n      <athlete-cards [splits]=\"race.splits\" [result]=\"result\"></athlete-cards>\n\n      <div class=\"row pt-4\">\n        <div class=\"col-md-6 col-xs-12\">\n          <div class=\"chart pt-4 pb-4 mb-4 euclides-background-color-1\">\n            <time-distribution-chart [splits]=\"race.splits\" [results]=\"result.splits\" [total]=\"result.total\" [showTransitions]=\"race.timeOnTransitions\"></time-distribution-chart>\n          </div>\n        </div>\n\n        <div class=\"chart col-md-6 col-xs-12\">\n          <div class=\"chart pt-4 pb-4 mb-4 euclides-background-color-1\">\n            <ranking-evolution [athlete]=\"result._id\" [race]=\"race\"></ranking-evolution>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row mb-4\" *ngIf=\"showPaceComparator\">\n        <div class=\"col-12\">\n          <div class=\"chart pt-4 pb-4 euclides-background-color-1\">\n            <running-pace-comparator [splits]=\"race.splits\" [results]=\"result.splits\"></running-pace-comparator>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row mb-4\" *ngIf=\"history && history.length > 1\">\n        <div class=\"col-12\">\n          <div class=\"chart pt-4 pb-4 euclides-background-color-1\">\n            <race-history [history]=\"history\" [currentYear]=\"result.year\" [race]=\"race\"></race-history>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row mb-4\">\n        <div class=\"col-12\">\n          <div class=\"chart pt-4 pb-4 euclides-background-color-1\">\n            <average-comparator [race]=\"race\" [result]=\"result\"></average-comparator>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row mb-4\" *ngIf=\"result.state == 'F'\">\n        <div class=\"col-12\">\n          <div class=\"chart pt-4 pb-4 euclides-background-color-1\">\n            <results-comparator [result]=\"result\" [race]=\"race\"></results-comparator>\n          </div>\n        </div>\n      </div>\n    </div>\n  </ng-container>\n\n  <div *ngIf=\"error\" id=\"error-box\" class=\"flex-row align-items-center euclides-background-color-2\">\n    <div class=\"container\">\n      <div class=\"row justify-content-center\">\n        <div class=\"col-lg-8\">\n            <div class=\"card p-4 euclides-background-color-1 euclides-border-color-6\">\n              <div class=\"card-body\">\n                <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>               \n                <p><a class=\"btn btn-info pull-right\" routerLink=\"..\">Volver</a></p>\n              </div>\n            </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ng-container>\n"

/***/ }),

/***/ "../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "h2 {\n  font-size: 1.2rem; }\n  h2 a {\n    letter-spacing: normal;\n    text-transform: none;\n    font-size: .7rem;\n    color: #fff;\n    cursor: pointer; }\n\n#athlete-selector,\n#error-box {\n  margin-top: 5rem; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MultisportAthleteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_item_model__ = __webpack_require__("../../../../../src/app/model/item.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MultisportAthleteComponent = (function () {
    function MultisportAthleteComponent(route, router, rankingService) {
        this.route = route;
        this.router = router;
        this.rankingService = rankingService;
        this.showPaceComparator = false;
        this.athletes = new Array();
        this.isLoading = true;
    }
    MultisportAthleteComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey)) {
            this.race = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey));
        }
        this.params = this.route.params.subscribe(function (params) {
            _this.athlete = params['athlete'];
            if (_this.athlete != undefined) {
                _this.getHistory(_this.athlete);
                if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].resultKey)) {
                    _this.result = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].resultKey));
                    if (_this.athlete != _this.result._id) {
                        _this.getResult(_this.athlete);
                    }
                    else {
                        _this.showPaceComparator = _this.checkIfShowPaceComparator(_this.race, _this.result);
                        _this.isLoading = false;
                    }
                }
                else {
                    _this.getResult(_this.athlete);
                }
            }
            else {
                _this.populateAthletes();
            }
        });
    };
    MultisportAthleteComponent.prototype.populateAthletes = function () {
        var _this = this;
        this.rankingService.getAthletesByEdition(this.race.edition).subscribe(function (athletes) {
            athletes.map(function (athlete) { return _this.athletes.push(new __WEBPACK_IMPORTED_MODULE_3__model_item_model__["a" /* Item */](athlete._id, athlete.name)); });
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    MultisportAthleteComponent.prototype.getResult = function (athlete) {
        var _this = this;
        this.rankingService.getResult(athlete).subscribe(function (result) {
            _this.result = result;
            _this.showPaceComparator = _this.checkIfShowPaceComparator(_this.race, _this.result);
            localStorage.setItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].resultKey, JSON.stringify(_this.result));
            localStorage.setItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].athleteKey, _this.result._id);
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) { return _this.error = error.status; });
    };
    MultisportAthleteComponent.prototype.getHistory = function (resultId) {
        var _this = this;
        this.rankingService.getResultHistory(resultId).subscribe(function (history) { return _this.history = history; });
    };
    MultisportAthleteComponent.prototype.checkIfShowPaceComparator = function (race, result) {
        return (race.sport == "duathlon" || (race.sport == "acuathlon" && race.splits[0].loop > 0)) &&
            race.splits[0].loop > 0 &&
            result.state == "F" &&
            result.splits[0] > 0 && result.splits[result.splits.length - 1] > 0;
    };
    MultisportAthleteComponent.prototype.selectAthlete = function (selectedAthlete) {
        this.selectedAthlete = selectedAthlete;
        this.noAthleteSelectedError = false;
    };
    MultisportAthleteComponent.prototype.goToAthlete = function () {
        if (this.selectedAthlete != undefined && this.selectAthlete != null) {
            this.noAthleteSelectedError = false;
            this.router.navigate(['/' + this.race.edition + '/athlete/' + this.selectedAthlete.id]);
        }
        else {
            this.noAthleteSelectedError = true;
        }
    };
    MultisportAthleteComponent.prototype.ngOnDestroy = function () {
        this.params.unsubscribe();
    };
    MultisportAthleteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'multisport-athlete',
            template: __webpack_require__("../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_4__service_ranking_ranking_service__["a" /* RankingService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__service_ranking_ranking_service__["a" /* RankingService */]])
    ], MultisportAthleteComponent);
    return MultisportAthleteComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/cards/athlete-cards/athlete-cards.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <ng-container *ngFor=\"let split of splits; let i = index;\">\n    <div *ngIf=\"splits[i].sport != 'transition'\" class=\"col-xs-6 col-lg-3 pt-4\">\n      <div [class]=\"split.sport\" [class.euclides-box-type-b-7]=\"true\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>\n          </div>      \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ split.sport | translate }} <span *ngIf=\"split.loop > 0\"> {{ splits[i].loop }}</span></p>\n            \n            <p class=\"box-description\">\n              {{ result.splits[i] | secondsToTime }}\n              \n              <span *ngIf=\"split.sport == 'swimming'\" class=\"pace\">\n                ({{ result.splits[i] | swimmingPace: split.distance }})\n              </span>\n              <span *ngIf=\"split.sport == 'running'\" class=\"pace\">\n                ({{ result.splits[i] | pace: split.distance }})\n              </span>\n              <span *ngIf=\"split.sport == 'cycling'\" class=\"pace\">\n                ({{ result.splits[i] | kmh: split.distance }})\n              </span>\n            </p>\n          </div>\n        </div> \n      </div>\n    </div>\n  </ng-container>\n  \n  <div class=\"col-xs-6 col-lg-3 pt-4\">\n    <div class=\"euclides-box-type-b-5 finish-line\">\n      <div class=\"box-body pb-0\">\n        <div class=\"box-icon\">\n          <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>\n        </div>      \n        <div class=\"box-content\">\n          <p class=\"box-title mb-0\">{{ \"total\" | translate }}</p>\n          <p class=\"box-description\"> {{ result.total | secondsToTime }}</p>\n        </div>\n      </div> \n    </div>\n  </div>  "

/***/ }),

/***/ "../../../../../src/app/dashboards/cards/athlete-cards/athlete-cards.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".running .box-icon {\n  background-image: url(\"/assets/icons/running.png\");\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: center center; }\n  .running .box-icon i {\n    display: none; }\n\n.cycling .box-icon {\n  background-image: url(\"/assets/icons/cycling.png\");\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: center center; }\n  .cycling .box-icon i {\n    display: none; }\n\n.swimming .box-icon {\n  background-image: url(\"/assets/icons/swimming.png\");\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: center center; }\n  .swimming .box-icon i {\n    display: none; }\n\n.finish-line .box-icon {\n  background-image: url(\"/assets/icons/finish-line.png\");\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: center center; }\n  .finish-line .box-icon i {\n    display: none; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/cards/athlete-cards/athlete-cards.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AthleteCardsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_result_model__ = __webpack_require__("../../../../../src/app/model/result.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AthleteCardsComponent = (function () {
    function AthleteCardsComponent() {
    }
    AthleteCardsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], AthleteCardsComponent.prototype, "splits", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__model_result_model__["a" /* Result */])
    ], AthleteCardsComponent.prototype, "result", void 0);
    AthleteCardsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'athlete-cards',
            template: __webpack_require__("../../../../../src/app/dashboards/cards/athlete-cards/athlete-cards.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/cards/athlete-cards/athlete-cards.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AthleteCardsComponent);
    return AthleteCardsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/cards/race-cards/race-cards.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row hidden-xs\">\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-8\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-arrow-right pull-right hidden-xs\" aria-hidden=\"true\"></i>\n          </div>       \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ distance }}</p>\n            <p class=\"box-description\">{{ 'dashboard.kilometers' | translate }}</p>\n          </div>\n        </div> \n      </div>\n    </div>\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-7\">\n        <div class=\"box-body pb-0\">\n            <div class=\"box-icon\">\n              <i class=\"fa fa-trophy hidden-xs\" aria-hidden=\"true\"></i>\n            </div>\n            <div class=\"box-content\">\n              <p class=\"box-title mb-0\">{{ athletes }}</p>\n              <p class=\"box-description\">{{ 'dashboard.athletes' | translate }}</p>\n            </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-5\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-male hidden-xs\" aria-hidden=\"true\"></i>\n          </div>           \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ men }}</p>\n            <p class=\"box-description\">{{ 'dashboard.men' | translate }}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-5\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-female hidden-xs\" aria-hidden=\"true\"></i>\n          </div>         \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ women }}</p>\n            <p class=\"box-description\">{{ 'dashboard.women' | translate }}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/cards/race-cards/race-cards.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/cards/race-cards/race-cards.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RaceCardsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RaceCardsComponent = (function () {
    function RaceCardsComponent() {
        this.athletes = 0;
        this.men = 0;
        this.women = 0;
        this.distance = 0;
    }
    RaceCardsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], RaceCardsComponent.prototype, "athletes", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], RaceCardsComponent.prototype, "men", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], RaceCardsComponent.prototype, "women", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], RaceCardsComponent.prototype, "distance", void 0);
    RaceCardsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'race-cards',
            template: __webpack_require__("../../../../../src/app/dashboards/cards/race-cards/race-cards.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/cards/race-cards/race-cards.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RaceCardsComponent);
    return RaceCardsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/cards/team-cards/team-cards.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row hidden-xs\">\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-8\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-users pull-right hidden-xs\" aria-hidden=\"true\"></i>\n          </div>       \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ teams }}</p>\n            <p class=\"box-description\">{{ 'dashboard.teams' | translate }}</p>\n          </div>\n        </div> \n      </div>\n    </div>\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-7\">\n        <div class=\"box-body pb-0\">\n            <div class=\"box-icon\">\n              <i class=\"fa fa-trophy hidden-xs\" aria-hidden=\"true\"></i>\n            </div>\n            <div class=\"box-content\">\n              <p class=\"box-title mb-0\">{{ athletes }}</p>\n              <p class=\"box-description\">{{ 'dashboard.athletes' | translate }}</p>\n            </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-5\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-user hidden-xs\" aria-hidden=\"true\"></i>\n          </div>           \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ notTeam }}</p>\n            <p class=\"box-description\">{{ 'dashboard.notTeam' | translate }}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-xs-6 col-lg-3 pt-4\">\n      <div class=\"euclides-box-type-a-5\">\n        <div class=\"box-body pb-0\">\n          <div class=\"box-icon\">\n            <i class=\"fa fa-user-o hidden-xs\" aria-hidden=\"true\"></i>\n          </div>         \n          <div class=\"box-content\">\n            <p class=\"box-title mb-0\">{{ notFederated }}</p>\n            <p class=\"box-description\">{{ 'dashboard.notFederated' | translate }}</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/cards/team-cards/team-cards.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/cards/team-cards/team-cards.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamCardsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TeamCardsComponent = (function () {
    function TeamCardsComponent() {
        this.athletes = 0;
        this.teams = 0;
        this.notTeam = 0;
        this.notFederated = 0;
    }
    TeamCardsComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], TeamCardsComponent.prototype, "athletes", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], TeamCardsComponent.prototype, "teams", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], TeamCardsComponent.prototype, "notTeam", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], TeamCardsComponent.prototype, "notFederated", void 0);
    TeamCardsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'team-cards',
            template: __webpack_require__("../../../../../src/app/dashboards/cards/team-cards/team-cards.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/cards/team-cards/team-cards.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TeamCardsComponent);
    return TeamCardsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/athletes-by-team/athletes-by-team.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n  <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n  <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div *ngIf=\"!isLoading\">\n  <h3 class=\"chart-title\">{{ 'dashboard.charts.title.athletes-by-team' | translate }}</h3>\n\n  <div class=\"alert alert-info ml-4 mr-4 mb-4\"> {{ 'dashboard.charts.athletes-by-team-info' | translate }}</div>\n\n  <canvas baseChart\n              [data]=\"data\"\n              [labels]=\"labels\"\n              [chartType]=\"chartType\"\n              [options]=\"options\"\n  >\n  </canvas>\n\n  <div *ngIf=\"error\" id=\"error-box\">\n    <div class=\"container-fluid\">\n      <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>                  \n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/athletes-by-team/athletes-by-team.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/athletes-by-team/athletes-by-team.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AthletesByTeamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AthletesByTeamComponent = (function () {
    function AthletesByTeamComponent(rankingService) {
        this.rankingService = rankingService;
        this.data = new Array();
        this.labels = new Array();
        this.chartType = "bar";
        this.isLoading = true;
    }
    AthletesByTeamComponent.prototype.ngOnInit = function () {
        this.generateCharData();
        this.createOptions();
    };
    AthletesByTeamComponent.prototype.generateCharData = function () {
        var _this = this;
        this.rankingService.countByTeam(this.edition).subscribe(function (teams) {
            teams.map(function (team) {
                _this.labels.push(team.team.name);
                _this.data.push(team.athletes);
            });
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    AthletesByTeamComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.split(" ").map(function (n) { return n[0]; }).join(".");
                            }
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.buildTooltip(_this.labels[tooltipItem.index], tooltipItem.yLabel);
                    }
                }
            }
        };
    };
    AthletesByTeamComponent.prototype.buildTooltip = function (label, value) {
        return label + ": " + value;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], AthletesByTeamComponent.prototype, "edition", void 0);
    AthletesByTeamComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'athletes-by-team',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/athletes-by-team/athletes-by-team.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/athletes-by-team/athletes-by-team.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__service_ranking_ranking_service__["a" /* RankingService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_ranking_ranking_service__["a" /* RankingService */]])
    ], AthletesByTeamComponent);
    return AthletesByTeamComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/average-comparator/average-comparator.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n  <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n  <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div *ngIf=\"!isLoading\">\n  <h3 class=\"chart-title\">{{ 'dashboard.charts.title.average-comparator' | translate }}</h3>\n\n  <ng-container *ngIf=\"!error\">\n    <form #filterForm=\"ngForm\">\n      <fieldset class=\"p-4\">\n        <label for=\"groupFilter\">{{ 'dashboard.charts.average' | translate }}</label>\n        <select [disabled]=\"isLoading\" class=\"form-control\" id=\"filter\" name=\"filter\" [(ngModel)]=\"filter\" (change)=\"updateDatasets()\">\n            <option value=\"all\"> {{ 'dashboard.charts.filter.all' | translate }} </option>\n            <option value=\"top__10\"> {{ 'dashboard.charts.filter.top10' | translate }} </option>\n            <option value=\"top__25\"> {{ 'dashboard.charts.filter.top25' | translate }} </option>\n            <option value=\"top__50\"> {{ 'dashboard.charts.filter.top50' | translate }} </option>\n            <option value=\"group__{{ result.group }}\"> {{ 'dashboard.charts.filter.group' | translate }} {{ result.group | translate }}</option>\n            <option value=\"category__{{ result.category }}\"> {{ 'dashboard.charts.filter.category' | translate }} {{ result.category }}</option>\n            <option value=\"team__{{ result.team.url }}\"> {{ 'dashboard.charts.filter.team' | translate }} {{ result.team.name }}</option>\n        </select>\n      </fieldset>\n    </form>\n\n    <div class=\"group\">\n      <div class=\"col-md-9 p-4 euclides-background-color-1\" >\n        <canvas baseChart\n                    [datasets]=\"datasets\"\n                    [labels]=\"labels\"\n                    [chartType]=\"chartType\"\n                    [options]=\"options\"\n        >\n        </canvas>\n      </div>\n\n      <div class=\"col-md-3 p-4 euclides-background-color-1\" >\n          <div *ngIf=\"result.state == 'F' && stats['totalInSeconds'] <= stats['avgTotalInSeconds']\" class=\"card text-white bg-success\">\n            <div class=\"card-body\">\n              <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.avg-up' | translate:stats\"></p>\n            </div>\n          </div>\n    \n          <div *ngIf=\"result.state == 'F' && stats['totalInSeconds'] > stats['avgTotalInSeconds'] && !checkSplits()\" class=\"card text-white bg-danger\">\n            <div class=\"card-body\">\n              <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.avg-down' | translate:stats\"></p>\n            </div>\n          </div>\n    \n          <div *ngIf=\"result.state == 'F' && stats['totalInSeconds'] > stats['avgTotalInSeconds'] && checkSplits()\" class=\"card text-white bg-warning\">\n              <div class=\"card-body\">\n                <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.avg-mid' | translate:stats\"></p>\n              </div>\n          </div>\n\n          <div *ngIf=\"result.state != 'F'\" class=\"card text-white bg-danger\">\n              <div class=\"card-body\">\n                <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.avg-null' | translate:stats\"></p>\n              </div>\n          </div>\n      </div>\n    </div>\n  </ng-container>\n  <div *ngIf=\"error\" id=\"error-box\">\n    <div class=\"container-fluid\">\n      <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>                  \n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/average-comparator/average-comparator.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/average-comparator/average-comparator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AverageComparatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_result_model__ = __webpack_require__("../../../../../src/app/model/result.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_race_model__ = __webpack_require__("../../../../../src/app/model/race.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AverageComparatorComponent = (function () {
    function AverageComparatorComponent(translateService, rankingService, timeConversionUtil) {
        this.translateService = translateService;
        this.rankingService = rankingService;
        this.timeConversionUtil = timeConversionUtil;
        this.mainDataset = {};
        this.datasets = new Array();
        this.labels = new Array();
        this.chartType = "line";
        this.stats = {};
        this.isLoading = true;
        this.filter = "all";
    }
    AverageComparatorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.showTransitions = this.race.timeOnTransitions;
        this.createLabels();
        this.createOptions();
        this.createMainDataset(function () { return _this.updateDatasets(); });
    };
    AverageComparatorComponent.prototype.updateDatasets = function () {
        var _this = this;
        this.isLoading = true;
        this.splitsChecked = undefined;
        this.datasets = new Array();
        this.datasets.push(this.mainDataset);
        var filterName = null;
        var filterValue = null;
        if (this.filter != "all") {
            var params = this.buildFilterParams(this.filter);
            filterName = params[0];
            filterValue = params[1];
        }
        this.rankingService.getAverage(this.race.edition, filterName, filterValue).subscribe(function (results) {
            var averageDataset = {};
            averageDataset["data"] = _this.filterTransitions(results.splits.map(function (split) { return split; }));
            averageDataset["data"].push(results.total);
            _this.translateService.get("dashboard.charts.group-average").subscribe(function (label) { return averageDataset["label"] = label; });
            _this.datasets.push(averageDataset);
            _this.stats["avgTotalInSeconds"] = results.total;
            _this.stats["avgTotal"] = _this.timeConversionUtil.secondsToTime(results.total);
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    AverageComparatorComponent.prototype.createMainDataset = function (callback) {
        this.mainDataset["data"] = this.filterTransitions(this.result.splits.map(function (split) { return split; }));
        this.mainDataset["data"].push(this.result.total);
        this.mainDataset["label"] = this.result.name;
        this.stats["totalInSeconds"] = this.result.total;
        this.stats["total"] = this.timeConversionUtil.secondsToTime(this.result.total);
        callback();
    };
    AverageComparatorComponent.prototype.createLabels = function () {
        var _this = this;
        this.race.splits.forEach(function (split, i) {
            if (_this.showTransitions || split.sport != "transition") {
                _this.translateService.get(split.sport).subscribe(function (label) {
                    if (split.loop > 0)
                        label += " " + split.loop;
                    _this.labels.push(label);
                });
            }
        });
        this.translateService.get("total").subscribe(function (label) { return _this.labels.push(label); });
    };
    AverageComparatorComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            maintainAspectRatio: true,
            reponsive: true,
            legend: {
                position: 'bottom'
            },
            elements: {
                line: {
                    tension: 0,
                    fill: false
                }
            },
            scales: {
                yAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return _this.timeConversionUtil.format(value, true);
                            }
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.buildTooltip(chart.datasets[tooltipItem.datasetIndex].label, tooltipItem.yLabel);
                    }
                }
            }
        };
    };
    AverageComparatorComponent.prototype.checkSplits = function () {
        var _this = this;
        if (this.splitsChecked == undefined) {
            var bestYear_1 = false;
            var statsSplits_1 = "";
            this.race.splits.forEach(function (split, i) {
                if (_this.datasets[0]["data"][i] < _this.datasets[1]["data"][i]) {
                    bestYear_1 = true;
                    statsSplits_1 += statsSplits_1.length == 0 ? _this.labels[i] : ", " + _this.labels[i];
                }
            });
            this.stats["segments"] = statsSplits_1;
            this.splitsChecked = bestYear_1;
        }
        return this.splitsChecked;
    };
    AverageComparatorComponent.prototype.buildTooltip = function (label, value) {
        return label + ": " + this.timeConversionUtil.format(value, true);
    };
    AverageComparatorComponent.prototype.filterTransitions = function (splits) {
        if (this.showTransitions) {
            return splits;
        }
        else {
            return splits.filter(function (split, i) { return i % 2 == 0; });
        }
    };
    AverageComparatorComponent.prototype.buildFilterParams = function (filter) {
        return filter.split("__");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__model_result_model__["a" /* Result */])
    ], AverageComparatorComponent.prototype, "result", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__model_race_model__["a" /* Race */])
    ], AverageComparatorComponent.prototype, "race", void 0);
    AverageComparatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'average-comparator',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/average-comparator/average-comparator.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/average-comparator/average-comparator.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_4__service_ranking_ranking_service__["a" /* RankingService */],
            __WEBPACK_IMPORTED_MODULE_5__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */]])
    ], AverageComparatorComponent);
    return AverageComparatorComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/race-history/race-history.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n    <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n    <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div *ngIf=\"!isLoading\">\n  <h3 class=\"chart-title\">{{ 'dashboard.charts.title.history-evolution' | translate }}</h3>\n  <div class=\"group\">\n    <div class=\"col-md-3 col-xs-12 p-4 euclides-background-color-1\" >\n      <div *ngIf=\"stats['total-best-year'] == currentYear\" class=\"card text-white bg-success\">\n        <div class=\"card-body\">\n          <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.race-history-up' | translate:stats\"></p>\n        </div>\n      </div>\n\n      <div *ngIf=\"stats['total-best-year'] != currentYear && !checkSplits()\" class=\"card text-white bg-danger\">\n        <div class=\"card-body\">\n          <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.race-history-down' | translate:stats\"></p>\n        </div>\n      </div>\n\n      <div *ngIf=\"stats['total-best-year'] != currentYear && checkSplits()\" class=\"card text-white bg-warning\">\n          <div class=\"card-body\">\n            <p class=\"card-text\" [innerHtml] =\"'dashboard.charts.race-history-mid' | translate:stats\"></p>\n          </div>\n      </div>\n    </div>\n\n    <div class=\"col-md-9 col-xs-12 p-4 euclides-background-color-1\" >\n      <canvas baseChart\n                  [datasets]=\"datasets\"\n                  [labels]=\"labels\"\n                  [chartType]=\"chartType\"\n                  [options]=\"options\"\n      >\n      </canvas>\n  </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/race-history/race-history.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/race-history/race-history.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RaceHistoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_race_model__ = __webpack_require__("../../../../../src/app/model/race.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RaceHistoryComponent = (function () {
    function RaceHistoryComponent(timeConversionUtil, translateService) {
        this.timeConversionUtil = timeConversionUtil;
        this.translateService = translateService;
        this.datasets = new Array();
        this.labels = new Array();
        this.chartType = "line";
        this.stats = {};
        this.isLoading = true;
    }
    RaceHistoryComponent.prototype.ngOnInit = function () {
        this.generateChartData();
        this.createOptions();
    };
    RaceHistoryComponent.prototype.generateChartData = function () {
        var _this = this;
        this.history.forEach(function (result, i) {
            _this.labels.push(result.year.toString());
            result.splits.forEach(function (split, n) {
                if (i == 0) {
                    var splitData_1 = {};
                    splitData_1["data"] = new Array();
                    _this.translateService.get(_this.race.splits[n].sport).subscribe(function (label) {
                        if (_this.race.splits[n].loop > 0)
                            label += " " + _this.race.splits[n].loop;
                        splitData_1["label"] = label;
                    });
                    _this.datasets.push(splitData_1);
                }
                _this.datasets[n]["data"].push(split);
                _this.checkStats(result, n.toString());
            });
            if (i == 0) {
                var totalData_1 = {};
                totalData_1["data"] = new Array();
                _this.translateService.get("total").subscribe(function (label) { return totalData_1["label"] = label; });
                _this.datasets.push(totalData_1);
            }
            _this.datasets[result.splits.length]["data"].push(result.total);
            _this.checkStats(result, "total");
        });
        this.isLoading = false;
    };
    RaceHistoryComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            maintainAspectRatio: true,
            reponsive: true,
            legend: {
                position: 'bottom'
            },
            elements: {
                line: {
                    tension: 0,
                    fill: false
                }
            },
            scales: {
                yAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return _this.timeConversionUtil.format(value, true);
                            }
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.buildTooltip(chart.datasets[tooltipItem.datasetIndex].label, tooltipItem.yLabel);
                    }
                }
            }
        };
    };
    /* Check if selected athlete has the best score in some segment */
    RaceHistoryComponent.prototype.checkSplits = function () {
        var _this = this;
        if (this.splitsChecked == undefined) {
            var bestYear_1 = false;
            var statsSplits_1 = "";
            this.race.splits.forEach(function (split, i) {
                if (_this.stats[i + "-best-year"] == _this.currentYear) {
                    bestYear_1 = true;
                    statsSplits_1 += statsSplits_1.length == 0 ? _this.datasets[i]["label"] : ", " + _this.datasets[i]["label"];
                }
            });
            this.stats["segments"] = statsSplits_1;
            this.splitsChecked = bestYear_1;
        }
        return this.splitsChecked;
    };
    RaceHistoryComponent.prototype.buildTooltip = function (label, value) {
        return label + ": " + this.timeConversionUtil.format(value, true);
    };
    RaceHistoryComponent.prototype.checkStats = function (result, split) {
        var currentValue = split == "total" ? result.total : result.splits[split];
        if (this.stats[split + "InSeconds"] == undefined || this.stats[split + "InSeconds"] > currentValue) {
            this.stats[split + "-best-year"] = result.year;
            this.stats[split] = this.timeConversionUtil.format(currentValue);
            this.stats[split + "InSeconds"] = currentValue;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], RaceHistoryComponent.prototype, "history", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], RaceHistoryComponent.prototype, "currentYear", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__model_race_model__["a" /* Race */])
    ], RaceHistoryComponent.prototype, "race", void 0);
    RaceHistoryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'race-history',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/race-history/race-history.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/race-history/race-history.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */], __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */],
            __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], RaceHistoryComponent);
    return RaceHistoryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/ranking-evolution/ranking-evolution.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n    <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n    <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div *ngIf=\"!isLoading\">\n  <h3 class=\"chart-title\">{{ 'dashboard.charts.title.ranking-evolution' | translate }}</h3>\n  <canvas *ngIf=\"!error\" baseChart\n              [datasets]=\"datasets\"\n              [labels]=\"labels\"\n              [chartType]=\"chartType\"\n              [options]=\"options\"\n  >\n  </canvas>\n\n  <div *ngIf=\"error\" id=\"error-box\">\n    <div class=\"container-fluid\">\n      <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>                  \n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/ranking-evolution/ranking-evolution.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/ranking-evolution/ranking-evolution.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RankingEvolutionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_race_model__ = __webpack_require__("../../../../../src/app/model/race.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RankingEvolutionComponent = (function () {
    function RankingEvolutionComponent(translateService, rankingService) {
        this.translateService = translateService;
        this.rankingService = rankingService;
        this.labels = new Array();
        this.chartType = "line";
        this.isLoading = true;
        this.datasets = new Array();
        this.numberOfAthletes = 0;
    }
    RankingEvolutionComponent.prototype.ngOnInit = function () {
        this.getAthleteRanking();
        this.createLabels();
    };
    RankingEvolutionComponent.prototype.getAthleteRanking = function () {
        var _this = this;
        this.rankingService.getAthleteRanking(this.athlete)
            .subscribe(function (ranking) {
            var partial = {};
            var global = {};
            partial["data"] = ranking.partial;
            global["data"] = ranking.global;
            _this.numberOfAthletes = ranking.total;
            _this.translateService.get("dashboard.charts.partial-rank").subscribe(function (label) { return partial["label"] = label; });
            _this.translateService.get("dashboard.charts.global-rank").subscribe(function (label) { return global["label"] = label; });
            // Fix any chrono issues using the total position for the last split
            global["data"][ranking.global.length - 2] = ranking.global[ranking.global.length - 1];
            _this.datasets.push(partial);
            _this.datasets.push(global);
            _this.createOptions();
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    RankingEvolutionComponent.prototype.createLabels = function () {
        var _this = this;
        this.race.splits.forEach(function (split, i) {
            _this.translateService.get(split.sport).subscribe(function (label) {
                if (split.loop > 0)
                    label += " " + split.loop;
                _this.labels.push(label);
            });
        });
    };
    RankingEvolutionComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            legend: {
                position: 'bottom'
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.buildTooltip(chart.datasets[tooltipItem.datasetIndex].label, tooltipItem.yLabel);
                    }
                }
            },
            elements: {
                line: {
                    tension: 0,
                    fill: false
                }
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: this.numberOfAthletes
                        }
                    }]
            }
        };
    };
    RankingEvolutionComponent.prototype.buildTooltip = function (label, value) {
        return label + ": " + value + "/" + this.numberOfAthletes;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__model_race_model__["a" /* Race */])
    ], RankingEvolutionComponent.prototype, "race", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], RankingEvolutionComponent.prototype, "athlete", void 0);
    RankingEvolutionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'ranking-evolution',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/ranking-evolution/ranking-evolution.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/ranking-evolution/ranking-evolution.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__["a" /* RankingService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__["a" /* RankingService */]])
    ], RankingEvolutionComponent);
    return RankingEvolutionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/results-comparator/results-comparator.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n  <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n  <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div id=\"athletes-comparator\" *ngIf=\"!isLoading\">\n  <ng-container *ngIf=\"!error\">\n    <h3 class=\"chart-title\">{{ 'dashboard.charts.title.athletes-comparator' | translate }}</h3>\n    <div class=\"row pl-4 pr-4\">\n      <div class=\"col-md-6 pl-4 pr-4\">\n          <ng-select\n            [items]=\"athletes\"\n            (selected)=\"addAthlete($event)\"\n            placeholder= \"{{ 'dashboard.charts.add-athlete' | translate }}\"\n          >\n          </ng-select>\n      </div>\n      <div class=\"col-md-6 pl-4 pr-4\">\n          <ng-select\n            [items]=\"teams\"\n            (selected)=\"addTeam($event)\"\n            placeholder= \"{{ 'dashboard.charts.add-team' | translate }}\"\n          >\n          </ng-select>\n      </div>\n    </div>\n\n    <div class=\"row pl-4 pr-4\" *ngIf=\"selectedAthletes.length == 0 && (!datasets || datasets.length == 0)\">\n      <div class=\"col-12\">\n        <div class=\"alert alert-info\" [innerHtml]=\"'dashboard.charts.athletes-comparator-info' | translate\"></div>\n      </div>\n    </div>\n\n    <div class=\"row pl-4 pr-4\" *ngIf=\"selectedAthletes.length > 0\">\n      <div class=\"col-12\">\n          <p class=\"badge badge-pill badge-info p-1 m-2\" *ngFor=\"let athlete of selectedAthletes\">\n            {{ athlete.text }}\n            <i class=\"fa fa-times actionable\" aria-hidden=\"true\" (click)=\"deleteAthlete(athlete.text)\"></i>\n          </p>\n      </div>\n      <div class=\"col-12 actions\">\n        <p class=\"pull-right\">\n          <a>\n            <button class=\"btn btn-danger actionable\" (click)=\"clearSelection()\">\n              {{ 'dashboard.charts.clear' | translate }}\n            </button>\n          </a>\n          <a>\n            <button class=\"btn btn-info actionable\" (click)=\"updateDatasets()\">\n              {{ 'dashboard.charts.update' | translate }}\n            </button>\n          </a>\n        </p>\n      </div>\n    </div>\n\n    <div *ngIf=\"datasets && datasets.length > 0\">\n      <div class=\"group\">\n        <div class=\"col-md-6 pb-4 euclides-background-color-1\" *ngFor=\"let dataset of datasets; let i = index;\">\n          <p>{{ titles[i] }}</p>\n          <canvas baseChart\n                      [data]=\"dataset.data\"\n                      [colors]=\"dataset.color\"\n                      [labels]=\"dataset.label\"\n                      [chartType]=\"chartType\"\n                      [options]=\"options\"\n          >\n          </canvas>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n\n\n    <div *ngIf=\"ranking && ranking.length > 0\">\n      <form #filterForm=\"ngForm\">\n          <div class=\"form-group euclides-background-color-1\">\n              <fieldset class=\"p-4\">\n                  <label for=\"disciplineFilter\">{{ 'dashboard.filters.by-discipline' | translate }}</label>\n                  <select class=\"form-control\" id=\"disciplineFilter\" name=\"rankingDiscipline\" [(ngModel)]=\"rankingDiscipline\" (change)=\"updateRanking()\">\n                      <option value=\"-1\"> {{ 'dashboard.filters.total' | translate }} </option>\n                      <ng-container *ngFor=\"let split of race.splits; let i = index;\">\n                          <option *ngIf=\"race.timeOnTransitions || split.sport !='transition'\" value=\"{{ i }}\"> \n                              {{ split.sport | translate }}\n                              <span *ngIf=\"split.loop > 0\">{{ split.loop }}</span>\n                          </option>\n                      </ng-container>\n                  </select>\n              </fieldset>\n          </div>\n      </form>\n\n      <multisport-ranking-table \n          [race]=\"race\" \n          [ranking]=\"ranking\" \n          [isLoading]=\"isLoading\"\n          [showSearch]=\"false\"\n          [navigateToDetail]=\"false\" \n          [order]=\"rankingDiscipline\">\n      </multisport-ranking-table>\n    </div>\n  </ng-container>\n  \n  <div *ngIf=\"error\" id=\"error-box\">\n    <div class=\"container-fluid\">\n      <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>                  \n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/results-comparator/results-comparator.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#athletes-comparator {\n  min-height: 30vh; }\n\n.actions {\n  margin-top: 2em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/results-comparator/results-comparator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultsComparatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_result_model__ = __webpack_require__("../../../../../src/app/model/result.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_race_model__ = __webpack_require__("../../../../../src/app/model/race.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_item_model__ = __webpack_require__("../../../../../src/app/model/item.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ALL_GROUPS = "global";
var ALL_CATEGORIES = "all-categories";
var ALL_TEAMS = "all-teams";
var ALL_SPLITS = -1;
var ResultsComparatorComponent = (function () {
    function ResultsComparatorComponent(rankingService, timeConversionUtil, translateService) {
        this.rankingService = rankingService;
        this.timeConversionUtil = timeConversionUtil;
        this.translateService = translateService;
        this.athletes = new Array();
        this.teams = new Array();
        this.selectedAthletes = new Array();
        this.chartType = "bar";
        this.othersColor = "rgba(255, 99, 132, 0.7)";
        this.myColor = "rgba(54, 162, 235, 0.7)";
        this.labels = new Array();
        this.titles = new Array();
        this.isLoading = true;
        this.rankingDiscipline = ALL_SPLITS;
    }
    ResultsComparatorComponent.prototype.ngOnInit = function () {
        this.populateAthletes();
        this.populateTeams();
        this.createTitles();
        this.createOptions();
    };
    ResultsComparatorComponent.prototype.populateAthletes = function () {
        var _this = this;
        this.rankingService.getAthletesByEdition(this.result.edition).subscribe(function (athletes) {
            athletes.map(function (athlete) {
                if (athlete.state == 'F') {
                    _this.athletes.push(new __WEBPACK_IMPORTED_MODULE_5__model_item_model__["a" /* Item */](athlete._id, athlete.name));
                }
            });
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    ResultsComparatorComponent.prototype.populateTeams = function () {
        var _this = this;
        if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey)) {
            JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey)).metadata.teams.map(function (team) {
                _this.teams.push(new __WEBPACK_IMPORTED_MODULE_5__model_item_model__["a" /* Item */](team.url, team.name));
            });
        }
    };
    ResultsComparatorComponent.prototype.addAthlete = function (selectedAthlete) {
        this.isLoading = true;
        if (selectedAthlete.id != this.result._id &&
            this.selectedAthletes.filter(function (athlete) { return athlete.id == selectedAthlete.id; }).length == 0) {
            this.selectedAthletes.push(selectedAthlete);
        }
        this.isLoading = false;
    };
    ResultsComparatorComponent.prototype.addTeam = function (selectedTeam) {
        var _this = this;
        this.isLoading = true;
        this.rankingService.getAthletesByEdition(this.result.edition, selectedTeam.id).subscribe(function (athletes) {
            athletes.map(function (athlete) {
                if (athlete.state == 'F') {
                    _this.addAthlete(new __WEBPACK_IMPORTED_MODULE_5__model_item_model__["a" /* Item */](athlete._id, athlete.name));
                }
            });
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    ResultsComparatorComponent.prototype.deleteAthlete = function (name) {
        this.selectedAthletes = this.selectedAthletes.filter(function (athlete) { return athlete.text != name; });
    };
    ResultsComparatorComponent.prototype.updateDatasets = function () {
        var _this = this;
        this.isLoading = true;
        this.datasets = new Array();
        this.labels = new Array();
        this.rankingDiscipline = ALL_SPLITS;
        var athletes;
        athletes = this.selectedAthletes.map(function (athlete) { return athlete.id; }).toString();
        this.rankingService.getRankingByEdition(this.race.edition, ALL_GROUPS, ALL_CATEGORIES, ALL_TEAMS, ALL_SPLITS, athletes + "," + this.result._id)
            .subscribe(function (results) {
            _this.ranking = results;
            var data = new Array(_this.ranking.length);
            var colors = new Array(_this.ranking.length);
            _this.ranking.forEach(function (result) {
                var i = 0;
                _this.race.splits.forEach(function (split, j) {
                    if (_this.race.timeOnTransitions || split.sport != 'transition') {
                        _this.populateDataset(i, result.splits[j], result.name, result._id == _this.result._id ? _this.myColor : _this.othersColor);
                        i++;
                    }
                });
                _this.populateDataset(i, result.total, result.name, result._id == _this.result._id ? _this.myColor : _this.othersColor);
            });
            _this.isLoading = false;
        }),
            function (error) {
                _this.isLoading = false;
            };
    };
    ResultsComparatorComponent.prototype.updateRanking = function () {
        var _this = this;
        this.isLoading = true;
        this.ranking.sort(function (a, b) {
            if (_this.rankingDiscipline == ALL_SPLITS) {
                if (a.total == 0)
                    return 1;
                if (b.total == 0)
                    return -1;
                return a.total > b.total ? 1 : -1;
            }
            else {
                if (a.splits[_this.rankingDiscipline] == 0)
                    return 1;
                if (b.splits[_this.rankingDiscipline] == 0)
                    return -1;
                return a.splits[_this.rankingDiscipline] > b.splits[_this.rankingDiscipline] ? 1 : -1;
            }
        });
        this.isLoading = false;
    };
    ResultsComparatorComponent.prototype.clearSelection = function () {
        this.selectedAthletes = new Array();
    };
    ResultsComparatorComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return _this.timeConversionUtil.format(value, false);
                            }
                        }
                    }],
                xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.split(" ").map(function (n) { return n[0]; }).join(".");
                            }
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.buildTooltip(_this.labels[tooltipItem.index], tooltipItem.yLabel);
                    }
                }
            }
        };
    };
    ResultsComparatorComponent.prototype.createTitles = function () {
        var _this = this;
        this.race.splits.forEach(function (split, i) {
            if (_this.race.timeOnTransitions || split.sport != "transition") {
                _this.translateService.get(split.sport).subscribe(function (label) {
                    if (split.loop > 0)
                        label += " " + split.loop;
                    _this.titles.push(label);
                });
            }
        });
        this.translateService.get("total").subscribe(function (label) { return _this.titles.push(label); });
    };
    ResultsComparatorComponent.prototype.buildTooltip = function (label, value) {
        return label + ": " + this.timeConversionUtil.format(value, true);
    };
    ResultsComparatorComponent.prototype.populateDataset = function (position, value, label, color) {
        if (this.datasets[position] == undefined) {
            this.datasets[position] = {};
            this.datasets[position]["data"] = new Array();
            this.datasets[position]["color"] = new Array(1);
            this.datasets[position]["color"][0] = {};
            this.datasets[position]["color"][0]["backgroundColor"] = new Array();
            this.datasets[position]["label"] = new Array();
        }
        this.datasets[position]["data"].push(value);
        this.datasets[position]["color"][0]["backgroundColor"].push(color);
        this.datasets[position]["label"].push(label);
        if (position == 0)
            this.labels.push(label);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__model_result_model__["a" /* Result */])
    ], ResultsComparatorComponent.prototype, "result", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__model_race_model__["a" /* Race */])
    ], ResultsComparatorComponent.prototype, "race", void 0);
    ResultsComparatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'results-comparator',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/results-comparator/results-comparator.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/results-comparator/results-comparator.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_6__service_ranking_ranking_service__["a" /* RankingService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__service_ranking_ranking_service__["a" /* RankingService */],
            __WEBPACK_IMPORTED_MODULE_7__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */],
            __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], ResultsComparatorComponent);
    return ResultsComparatorComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/running-pace-comparator/running-pace-comparator.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n    <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n    <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div *ngIf=\"!isLoading\">\n  <h3 class=\"chart-title\">{{ 'dashboard.charts.title.running-pace-comparator' | translate }}</h3>\n  <div class=\"group\">\n    <div class=\"col-md-6 col-xs-12 pb-4 euclides-background-color-1\">\n      <canvas baseChart\n                  [data]=\"data\"\n                  [labels]=\"labels\"\n                  [chartType]=\"chartType\"\n                  [options]=\"options\"\n      >\n      </canvas>\n    </div>\n\n    <div class=\"col-md-6 col-xs-12 p-4 euclides-background-color-1\" >\n      <div class=\"card text-white euclides-background-color-6\">\n        <div class=\"card-body\">\n          <p class=\"card-text\" *ngIf=\"stats.pace1InSeconds > stats.pace2InSeconds\" [innerHtml] =\"'dashboard.charts.pace-comparation-split2' | translate:stats\"></p>\n          <p class=\"card-text\" *ngIf=\"stats.pace2InSeconds > stats.pace1InSeconds\" [innerHtml] =\"'dashboard.charts.pace-comparation-split1' | translate:stats\"></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/running-pace-comparator/running-pace-comparator.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/running-pace-comparator/running-pace-comparator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RunningPaceComparatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RunningPaceComparatorComponent = (function () {
    function RunningPaceComparatorComponent(translateService, timeConversionUtil) {
        this.translateService = translateService;
        this.timeConversionUtil = timeConversionUtil;
        this.data = new Array();
        this.labels = new Array();
        this.tooltips = new Array();
        this.chartType = "bar";
        this.stats = {};
        this.isLoading = true;
    }
    RunningPaceComparatorComponent.prototype.ngOnInit = function () {
        this.createOptions();
        this.generateCharDataAndLabelsAndTooltips();
    };
    RunningPaceComparatorComponent.prototype.generateCharDataAndLabelsAndTooltips = function () {
        var _this = this;
        var loop = 0;
        this.splits.forEach(function (split, i) {
            if (split.sport == "running") {
                loop += 1;
                var paceInSeconds = _this.timeConversionUtil.timeToPaceInSeconds(_this.results[i], split.distance);
                var pace = _this.timeConversionUtil.format(paceInSeconds, false);
                _this.data.push(paceInSeconds);
                _this.tooltips.push(pace);
                _this.stats["pace" + loop + "InSeconds"] = paceInSeconds;
                _this.stats["pace" + loop] = pace;
                _this.translateService.get(split.sport).subscribe(function (label) {
                    _this.labels.push(label + " " + split.loop);
                    if (i == _this.splits.length - 1)
                        _this.isLoading = false;
                });
            }
        });
        this.stats["seconds"] = this.stats.pace1InSeconds > this.stats.pace2InSeconds ?
            (this.stats.pace1InSeconds - this.stats.pace2InSeconds).toFixed(2) :
            (this.stats.pace2InSeconds - this.stats.pace1InSeconds).toFixed(2);
        this.stats["percentage"] = this.timeConversionUtil.differenceInPercentage(this.stats.pace1InSeconds, this.stats.pace2InSeconds);
    };
    RunningPaceComparatorComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            stepSize: 5,
                            callback: function (value, index, values) {
                                return _this.timeConversionUtil.format(value, false);
                            }
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.tooltips[tooltipItem.index];
                    }
                }
            }
        };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], RunningPaceComparatorComponent.prototype, "splits", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], RunningPaceComparatorComponent.prototype, "results", void 0);
    RunningPaceComparatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'running-pace-comparator',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/running-pace-comparator/running-pace-comparator.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/running-pace-comparator/running-pace-comparator.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */]])
    ], RunningPaceComparatorComponent);
    return RunningPaceComparatorComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/charts/time-distribution-chart/time-distribution-chart.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"isLoading\">\n    <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n    <span class=\"sr-only\">Loading...</span>\n</div>\n\n<div *ngIf=\"!isLoading\">\n  <h3 class=\"chart-title\">{{ 'dashboard.charts.title.time-distribution' | translate }}</h3>\n  <canvas baseChart\n              [data]=\"results\"\n              [labels]=\"labels\"\n              [chartType]=\"chartType\"\n              [options]=\"options\"\n  >\n  </canvas>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/time-distribution-chart/time-distribution-chart.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/charts/time-distribution-chart/time-distribution-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeDistributionChartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TimeDistributionChartComponent = (function () {
    function TimeDistributionChartComponent(translateService, timeConversionUtil) {
        this.translateService = translateService;
        this.timeConversionUtil = timeConversionUtil;
        this.labels = new Array();
        this.tooltips = new Array();
        this.chartType = "doughnut";
        this.isLoading = true;
    }
    TimeDistributionChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.checkTransitions(function () {
            _this.createLabelsAndTooltips();
            _this.createOptions();
        });
    };
    TimeDistributionChartComponent.prototype.checkTransitions = function (callback) {
        var _this = this;
        if (!this.showTransitions) {
            var splits_1 = new Array();
            var results_1 = new Array();
            this.splits.forEach(function (split, i) {
                if (split.sport != 'transition') {
                    splits_1.push(split);
                    results_1.push(_this.results[i]);
                }
            });
            this.splits = splits_1;
            this.results = results_1;
        }
        callback();
    };
    TimeDistributionChartComponent.prototype.createLabelsAndTooltips = function () {
        var _this = this;
        this.splits.forEach(function (split, i) {
            _this.translateService.get(split.sport).subscribe(function (label) {
                if (split.loop > 0)
                    label += " " + split.loop;
                _this.tooltips.push(_this.buildTooltip(label, _this.results[i]));
                _this.labels.push(label);
                if (i == _this.splits.length - 1)
                    _this.isLoading = false;
            });
        });
    };
    TimeDistributionChartComponent.prototype.createOptions = function () {
        var _this = this;
        this.options = {
            legend: {
                position: 'bottom'
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, chart) {
                        return _this.tooltips[tooltipItem.index];
                    }
                }
            }
        };
    };
    TimeDistributionChartComponent.prototype.buildTooltip = function (label, result) {
        return label + ": " +
            this.timeConversionUtil.timePercentage(result, this.total) +
            " (" + this.timeConversionUtil.secondsToTime(result) + ")";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], TimeDistributionChartComponent.prototype, "splits", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], TimeDistributionChartComponent.prototype, "results", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], TimeDistributionChartComponent.prototype, "total", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], TimeDistributionChartComponent.prototype, "showTransitions", void 0);
    TimeDistributionChartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'time-distribution-chart',
            template: __webpack_require__("../../../../../src/app/dashboards/charts/time-distribution-chart/time-distribution-chart.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/charts/time-distribution-chart/time-distribution-chart.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */]])
    ], TimeDistributionChartComponent);
    return TimeDistributionChartComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard-menu/dashboard-menu.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md navbar-light bg-light\" [class.fixed-top]=\"fixed\" id=\"dashboard-nav\">\n    <button class=\"navbar-toggler pull-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#dashboard-menu\" aria-controls=\"dashboard-menu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n    </button>\n  \n  <div class=\"navbar-brand\">\n    <img src=\"/assets/imgs/logo.png\" class=\"pull-left\" />\n    <h1 class=\"navbar-brand\"> \n      {{ race.name }}\n      <a class=\"badge badge-pill badge-success p-1\" data-toggle=\"modal\" data-target=\"#race-preferences\">\n        {{ 'dashboard.setup' | translate }} \n        <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>\n      </a>\n\n      <a class=\"badge badge-pill badge-info p-1\" routerLink=\"/\">\n        {{ 'dashboard.change-it' | translate }} \n        <i class=\"fa fa-pencil-square\" aria-hidden=\"true\"></i>\n      </a>\n    </h1>\n  </div>\n\n  <div class=\"collapse navbar-collapse\" id=\"dashboard-menu\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"ranking\" routerLinkActive=\"active\">\n          <i class=\"fa fa-sort-amount-asc\" aria-hidden=\"true\"></i>\n          {{ 'dashboard.menu.ranking' | translate }}\n        </a>\n      </li>\n      <li *ngIf=\"!athlete\" class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"athlete\" routerLinkActive=\"active\">\n          <i class=\"fa fa-bar-chart\" aria-hidden=\"true\"></i>            \n          {{ 'dashboard.menu.athlete-stats' | translate }}\n        </a>\n      </li>\n      <li *ngIf=\"athlete\" class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"athlete/{{athlete}}\" [class.active]=\"router.isActive('/' + race.edition + '/athlete', false)\">\n          <i class=\"fa fa-bar-chart\" aria-hidden=\"true\"></i>            \n          {{ 'dashboard.menu.athlete-stats' | translate }}\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"teams\" routerLinkActive=\"active\">\n          <i class=\"fa fa-users\" aria-hidden=\"true\"></i>            \n          {{ 'dashboard.menu.teams' | translate }}\n        </a>\n      </li>\n    </ul>\n  </div>\n  <div class=\"clearfix\"></div>\n</nav>\n<div [class.fixscroll]=\"fixed\"></div>\n\n<div id=\"race-preferences\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\">{{ 'dashboard.preferences.distances' | translate }}</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <p>{{ 'dashboard.preferences.text' | translate }}</p>\n\n          <div *ngIf=\"!distancesOk\" class=\"alert alert-danger\">\n              <strong>{{ 'dashboard.errors.not-valid' | translate }}</strong>\n          </div>\n\n          <form>\n            <div *ngFor=\"let split of race.splits; let i = index;\" class=\"form-group\">\n              <ng-container *ngIf=\"split.sport != 'transition'\">\n                <label for=\"{{ 'split' + (i + 1)}}\">\n                  {{ split.sport | translate }}\n                  <span *ngIf=\"split.loop > 0\">{{ split.loop }}</span>\n                </label>\n                <input type=\"number\" min=\"0\" required class=\"form-control\" id=\"{{ 'split' + (i + 1)}}\" name=\"{{ 'split' + (i + 1)}}\" [(ngModel)]=\"split.distance\">\n                <small class=\"form-text text-muted\">{{ 'dashboard.preferences.meters' | translate }}</small>\n              </ng-container>\n            </div>\n          </form>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-info\" (click)=\"updateDistances()\">{{ 'dashboard.preferences.refresh' | translate }}</button>\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{ 'dashboard.preferences.close' | translate }}</button>\n        </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard-menu/dashboard-menu.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "nav.navbar {\n  display: block;\n  padding: .5rem 1rem; }\n  nav.navbar .navbar-brand {\n    display: block;\n    white-space: normal;\n    padding: 0; }\n    nav.navbar .navbar-brand img {\n      max-width: 4rem;\n      margin-right: 1.5rem; }\n    nav.navbar .navbar-brand h1 {\n      padding-top: .5em; }\n      nav.navbar .navbar-brand h1 a {\n        letter-spacing: normal;\n        text-transform: none;\n        font-size: .7rem;\n        color: #fff;\n        cursor: pointer; }\n    nav.navbar .navbar-brand a {\n      padding: 0; }\n  nav.navbar .navbar-toggler {\n    top: 1em;\n    clear: both;\n    cursor: pointer; }\n  nav.navbar.fixed-top .navbar-brand {\n    display: none; }\n  nav.navbar.fixed-top #dashboard-menu {\n    margin-left: 5.5rem; }\n\n.navbar-expand-md .navbar-nav .nav-link {\n  padding: 0; }\n\n.fixscroll {\n  height: 100px; }\n\n.modal {\n  background-color: rgba(0, 0, 0, 0.7); }\n  .modal button {\n    cursor: pointer; }\n\n@media (max-width: 767px) {\n  nav.navbar ul li.nav-item {\n    margin-left: 0; }\n  nav.navbar .navbar-brand h1 {\n    padding-top: 0; }\n  .navbar-collapse {\n    background-color: #f2f4f8;\n    padding: .5em;\n    margin-top: 2rem;\n    clear: both; }\n  .fixscroll {\n    height: 140px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard-menu/dashboard-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardMenuComponent = (function () {
    function DashboardMenuComponent(route, router) {
        this.route = route;
        this.router = router;
        this.fixed = false;
        this.distancesOk = true;
    }
    DashboardMenuComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey)) {
            this.race = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey));
        }
    };
    DashboardMenuComponent.prototype.ngDoCheck = function () {
        var athlete = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].athleteKey);
        if (athlete != undefined) {
            this.athlete = athlete;
        }
    };
    DashboardMenuComponent.prototype.updateDistances = function () {
        var _this = this;
        var total = 0;
        this.distancesOk = true;
        this.race.splits.map(function (split) {
            if (split.sport != "transition") {
                if (!Number.isInteger(split.distance) || split.distance <= 0) {
                    _this.distancesOk = false;
                }
                else {
                    total += split.distance;
                }
            }
        });
        if (this.distancesOk) {
            this.race.metadata.distance = total / 1000;
            localStorage.setItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey, JSON.stringify(this.race));
            location.reload();
        }
    };
    DashboardMenuComponent.prototype.onWindowScroll = function () {
        if (window.scrollY > 100) {
            this.fixed = true;
        }
        else {
            this.fixed = false;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])("window:scroll", []),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DashboardMenuComponent.prototype, "onWindowScroll", null);
    DashboardMenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dashboard-menu',
            template: __webpack_require__("../../../../../src/app/dashboards/dashboard-menu/dashboard-menu.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/dashboard-menu/dashboard-menu.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], DashboardMenuComponent);
    return DashboardMenuComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"error\" id=\"error-box\" class=\"flex-row align-items-center euclides-background-color-2\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-lg-8\">\n          <div class=\"card p-4 euclides-background-color-1 euclides-border-color-6\">\n            <div class=\"card-body\">\n              <p *ngIf=\"error == 404\" class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.no-such-race' | translate }}</p>\n              <p *ngIf=\"error == 500\" class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }}</p>\n              \n              <p><a class=\"btn btn-info pull-right\" routerLink=\"/\">Volver</a></p>\n            </div>\n          </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div id=\"load-progress\" *ngIf=\"load != '100%'\">\n  <div class=\"progress\">\n      <div class=\"progress-bar progress-bar-striped progress-bar-animated bg-info\" role=\"progressbar\" \n          [style.width]=\"load\" [attr.aria-valuenow]=\"load\" aria-valuemin=\"0\" aria-valuemax=\"100\">\n      </div>\n  </div>\n</div>\n\n<div class=\"content\" *ngIf=\"race && load == '100%'\">\n  <dashboard-menu></dashboard-menu>\n  <router-outlet> </router-outlet>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div#error-box {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh; }\n\ndiv#load-progress {\n  margin-top: 50vh; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_race_metadata_model__ = __webpack_require__("../../../../../src/app/model/race-metadata.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_race_race_service__ = __webpack_require__("../../../../../src/app/service/race/race.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DashboardComponent = (function () {
    function DashboardComponent(route, router, raceService, rankingService) {
        this.route = route;
        this.router = router;
        this.raceService = raceService;
        this.rankingService = rankingService;
        this.load = "0%";
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.params = this.route.params.subscribe(function (params) {
            _this.edition = params['id'];
            if (_this.edition != undefined) {
                _this.load = "10%";
                if (localStorage.getItem("race")) {
                    _this.race = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey));
                    if (_this.race.edition == _this.edition)
                        _this.load = "100%";
                    else
                        _this.getRace(_this.edition);
                }
                else
                    _this.getRace(_this.edition);
            }
            else {
                _this.router.navigate(['/']);
            }
        });
    };
    DashboardComponent.prototype.getRace = function (edition) {
        var _this = this;
        this.raceService.getRaceByEdition(edition)
            .subscribe(function (race) {
            _this.race = race;
            _this.load = "30%";
            _this.getResultsMetadata(race.edition);
        }, function (error) { return _this.error = error.status; });
    };
    DashboardComponent.prototype.getResultsMetadata = function (edition) {
        var _this = this;
        this.race.metadata = new __WEBPACK_IMPORTED_MODULE_3__model_race_metadata_model__["a" /* RaceMetadata */](0, 0, 0, 0, 0, 0, 0, new Array(), new Array(), new Array());
        this.rankingService.getTeamsAndCategories(edition)
            .subscribe(function (data) {
            _this.load = "60%";
            _this.race.metadata.groups = data.groups.sort();
            _this.race.metadata.categories = data.categories.sort();
            _this.race.metadata.notFederated = data.notFederated;
            _this.race.metadata.notTeam = data.notTeam;
            _this.race.metadata.women = data.women;
            _this.race.metadata.men = data.men;
            _this.race.metadata.athletes = _this.race.metadata.men + _this.race.metadata.women;
            _this.race.metadata.teams = data.teams.sort(function (a, b) {
                if (a.name < b.name)
                    return -1;
                else
                    return 1;
            });
            _this.race.metadata.numberOfTeams = data.teams.filter(function (team) { return !__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].notFederated.includes(team.url) && !__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].notTeam.includes(team.url); }).length;
            _this.race.metadata.distance = _this.calculateRaceDistance(_this.race);
            _this.race.metadata.distance = _this.calculateRaceDistance(_this.race);
            localStorage.setItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey, JSON.stringify(_this.race));
            _this.load = "100%";
        }, function (error) { return _this.error = error.status; });
    };
    /** Distance in kilometers */
    DashboardComponent.prototype.calculateRaceDistance = function (race) {
        var distance = 0;
        for (var i in race.splits) {
            distance += +race.splits[i].distance;
        }
        return distance / 1000;
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        this.params.unsubscribe();
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dashboard',
            template: __webpack_require__("../../../../../src/app/dashboards/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/dashboard.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_4__service_race_race_service__["a" /* RaceService */], __WEBPACK_IMPORTED_MODULE_5__service_ranking_ranking_service__["a" /* RankingService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__service_race_race_service__["a" /* RaceService */],
            __WEBPACK_IMPORTED_MODULE_5__service_ranking_ranking_service__["a" /* RankingService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_modules_translate_shared_module_translate_shared_module_module__ = __webpack_require__("../../../../../src/app/shared/modules/translate-shared-module/translate-shared-module.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__routing_app_route_module__ = __webpack_require__("../../../../../src/app/dashboards/routing/app-route.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_charts__ = __webpack_require__("../../../../ng2-charts/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_select__ = __webpack_require__("../../../../ng2-select/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_ng2_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__shared_pipes_seconds_to_time_pipe__ = __webpack_require__("../../../../../src/app/shared/pipes/seconds-to-time.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared_pipes_pace_pipe__ = __webpack_require__("../../../../../src/app/shared/pipes/pace.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_pipes_swimming_pace_pipe__ = __webpack_require__("../../../../../src/app/shared/pipes/swimming-pace.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__shared_pipes_kmh_pipe__ = __webpack_require__("../../../../../src/app/shared/pipes/kmh.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__dashboard_component__ = __webpack_require__("../../../../../src/app/dashboards/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__dashboard_menu_dashboard_menu_component__ = __webpack_require__("../../../../../src/app/dashboards/dashboard-menu/dashboard-menu.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ranking_multisport_ranking_multisport_ranking_component__ = __webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__cards_race_cards_race_cards_component__ = __webpack_require__("../../../../../src/app/dashboards/cards/race-cards/race-cards.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__athlete_multisport_athlete_multisport_athlete_component__ = __webpack_require__("../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__cards_athlete_cards_athlete_cards_component__ = __webpack_require__("../../../../../src/app/dashboards/cards/athlete-cards/athlete-cards.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__charts_time_distribution_chart_time_distribution_chart_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/time-distribution-chart/time-distribution-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__charts_ranking_evolution_ranking_evolution_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/ranking-evolution/ranking-evolution.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__charts_running_pace_comparator_running_pace_comparator_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/running-pace-comparator/running-pace-comparator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__charts_race_history_race_history_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/race-history/race-history.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__charts_average_comparator_average_comparator_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/average-comparator/average-comparator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__charts_results_comparator_results_comparator_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/results-comparator/results-comparator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ranking_multisport_ranking_table_multisport_ranking_table_component__ = __webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking-table/multisport-ranking-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__team_multisport_team_multisport_team_component__ = __webpack_require__("../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__cards_team_cards_team_cards_component__ = __webpack_require__("../../../../../src/app/dashboards/cards/team-cards/team-cards.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__charts_athletes_by_team_athletes_by_team_component__ = __webpack_require__("../../../../../src/app/dashboards/charts/athletes-by-team/athletes-by-team.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
































var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_router__["c" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_9_ng2_charts__["ChartsModule"],
                __WEBPACK_IMPORTED_MODULE_7__routing_app_route_module__["a" /* AppRouteModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_modules_translate_shared_module_translate_shared_module_module__["a" /* TranslateSharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_10_ng2_select__["SelectModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_16__dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_17__dashboard_menu_dashboard_menu_component__["a" /* DashboardMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_18__ranking_multisport_ranking_multisport_ranking_component__["a" /* MultisportRankingComponent */],
                __WEBPACK_IMPORTED_MODULE_19__cards_race_cards_race_cards_component__["a" /* RaceCardsComponent */],
                __WEBPACK_IMPORTED_MODULE_12__shared_pipes_seconds_to_time_pipe__["a" /* SecondsToTimePipe */],
                __WEBPACK_IMPORTED_MODULE_13__shared_pipes_pace_pipe__["a" /* PacePipe */],
                __WEBPACK_IMPORTED_MODULE_14__shared_pipes_swimming_pace_pipe__["a" /* SwimmingPacePipe */],
                __WEBPACK_IMPORTED_MODULE_15__shared_pipes_kmh_pipe__["a" /* KmhPipe */],
                __WEBPACK_IMPORTED_MODULE_20__athlete_multisport_athlete_multisport_athlete_component__["a" /* MultisportAthleteComponent */],
                __WEBPACK_IMPORTED_MODULE_21__cards_athlete_cards_athlete_cards_component__["a" /* AthleteCardsComponent */],
                __WEBPACK_IMPORTED_MODULE_22__charts_time_distribution_chart_time_distribution_chart_component__["a" /* TimeDistributionChartComponent */],
                __WEBPACK_IMPORTED_MODULE_23__charts_ranking_evolution_ranking_evolution_component__["a" /* RankingEvolutionComponent */],
                __WEBPACK_IMPORTED_MODULE_24__charts_running_pace_comparator_running_pace_comparator_component__["a" /* RunningPaceComparatorComponent */],
                __WEBPACK_IMPORTED_MODULE_25__charts_race_history_race_history_component__["a" /* RaceHistoryComponent */],
                __WEBPACK_IMPORTED_MODULE_26__charts_average_comparator_average_comparator_component__["a" /* AverageComparatorComponent */],
                __WEBPACK_IMPORTED_MODULE_27__charts_results_comparator_results_comparator_component__["a" /* ResultsComparatorComponent */],
                __WEBPACK_IMPORTED_MODULE_28__ranking_multisport_ranking_table_multisport_ranking_table_component__["a" /* MultisportRankingTableComponent */],
                __WEBPACK_IMPORTED_MODULE_29__team_multisport_team_multisport_team_component__["a" /* MultisportTeamComponent */],
                __WEBPACK_IMPORTED_MODULE_30__cards_team_cards_team_cards_component__["a" /* TeamCardsComponent */],
                __WEBPACK_IMPORTED_MODULE_31__charts_athletes_by_team_athletes_by_team_component__["a" /* AthletesByTeamComponent */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_16__dashboard_component__["a" /* DashboardComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_11__shared_utils_time_conversion_utils__["a" /* TimeConversionUtil */], __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/ranking/multisport-ranking-table/multisport-ranking-table.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"ranking\" class=\"container-fluid pt-4\">\n    <div class=\"loading\" *ngIf=\"isLoading\">\n        <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n        <span class=\"sr-only\">Loading...</span>\n    </div>\n\n    <ng-container *ngIf=\"!isLoading\">\n        <div *ngIf=\"ranking && ranking.length == 0\" class=\"alert alert-danger\">\n            <strong>{{ 'dashboard.errors.no-results' | translate }}</strong>\n        </div>\n\n        <div class=\"row\" *ngIf=\"showSearch\">\n            <div class=\"col-12\">\n                <div class=\"input-group mb-4\">\n                    <input class=\"form-control\" [disabled]=\"isLoading\" [attr.placeholder]=\"'dashboard.search-by-athlete' | translate\" name=\"search\" id=\"search\" type=\"text\" \n                        [(ngModel)]=\"search\" (keyup)=\"updateRanking()\">\n                    <div class=\"input-group-btn\">\n                        <button class=\"btn btn-default\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i></button>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <table class=\"table table-hover table-bordered table-sm euclides-background-color-1\">\n            <thead class=\"euclides-background-color-6\">\n                <tr>\n                    <th></th>\n                    <th>{{ 'name' | translate }}</th>\n                    <ng-container *ngIf=\"!isMobile\">\n                        <th>{{ 'club' | translate }}</th>\n                        <ng-container *ngFor=\"let split of race.splits\">\n                            <th *ngIf=\"split.sport != 'transition' || race.timeOnTransitions\">\n                                {{ split.sport | translate }}\n                                <span *ngIf=\"split.loop > 0\">{{ split.loop }}</span>\n                            </th>\n                        </ng-container>\n                        <th>{{ 'total' | translate }}</th>\n                    </ng-container>\n                </tr>\n            </thead>\n\n            <tbody>\n                <ng-container *ngFor=\"let result of ranking; let i = index\">\n                    <tr *ngIf=\"!filterRanking || result.name.toUpperCase().includes(search.toUpperCase())\"\n                        [class.table-warning]=\"result.state == 'NF'\" \n                        [class.table-danger]=\"result.state == 'NDSQ'\" \n                        [class.table-info]=\"result.state == 'NP'\"\n                        [class.actionable]=\"!isMobile\"\n                        (click) = \"isMobile || !navigateToDetail || goToDetail(result._id)\"\n                    >\n                        <td>\n                            <span *ngIf=\"result.state == 'F'\">{{ i + 1 }}</span>\n                            <span *ngIf=\"result.state != 'F'\">{{ result.state }}</span>\n                        </td>\n                        <td>{{ result.name }}\n                            <span *ngIf=\"isMobile\" class=\"total\">\n                                <a (click)=\"showDetail(result._id)\">\n                                    <span *ngIf=\"order < 0\">{{ 'total' | translate }}: {{ result.total | secondsToTime }}</span>\n                                    <span *ngIf=\"order >= 0\">\n                                            {{ race.splits[order].sport | translate }}\n                                            <span *ngIf=\"race.splits[order].loop > 0\">{{ race.splits[order].loop }}</span>: \n                                            {{ result.splits[order] | secondsToTime }}\n                                    </span>\n                                    <i *ngIf=\"detail != result._id\" class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>      \n                                    <i *ngIf=\"detail == result._id\" class=\"fa fa-minus-square\" aria-hidden=\"true\"></i>\n                                </a>\n                            </span>\n\n                            <ng-container *ngIf=\"detail == result._id\">\n                                <dl>\n                                    <dt>{{ 'club' | translate }}</dt>\n                                    <dd>{{ result.team.name }}</dd>\n\n                                    <ng-container *ngFor=\"let split of result.splits; let i = index\">\n                                        <ng-container *ngIf=\"race.splits[i].sport != 'transition' || race.timeOnTransitions\">\n                                            <dt>\n                                                {{ race.splits[i].sport | translate }}\n                                                <span *ngIf=\"race.splits[i].loop > 0\">{{ race.splits[i].loop }}</span>\n                                            </dt>\n                                            <dd>\n                                                {{ split | secondsToTime }}\n                                                <span *ngIf=\"race.splits[i].sport == 'swimming'\" class=\"pace\">\n                                                    ({{ split | swimmingPace: race.splits[i].distance }})\n                                                </span>\n                                                <span *ngIf=\"race.splits[i].sport == 'running'\" class=\"pace\">\n                                                    ({{ split | pace: race.splits[i].distance }})\n                                                </span>\n                                                <span *ngIf=\"race.splits[i].sport == 'cycling'\" class=\"pace\">\n                                                    ({{ split | kmh: race.splits[i].distance }})\n                                                </span>\n                                            </dd>\n                                        </ng-container>\n                                    </ng-container>\n\n                                    <dt>{{ 'total' | translate }}</dt>\n                                    <dd class=\"last\">{{ result.total | secondsToTime }}</dd>\n                                </dl>\n\n                                <button *ngIf=\"navigateToDetail\" type=\"button\" class=\"btn btn-info pull-right\" (click)=\"goToDetail(result._id)\">{{ 'dashboard.show-stats' | translate }}</button>\n                            </ng-container>\n                        </td>\n                        <ng-container *ngIf=\"!isMobile\">\n                            <td>{{ result.team.name }}</td>\n                            <ng-container *ngFor=\"let split of result.splits; let i = index\">\n                                <td *ngIf=\"race.splits[i].sport != 'transition' || race.timeOnTransitions\">\n                                    {{ split | secondsToTime }}\n                                    <span *ngIf=\"race.splits[i].sport == 'running'\" class=\"pace\">({{ split | pace: race.splits[i].distance }})</span>\n                                    <span *ngIf=\"race.splits[i].sport == 'swimming'\" class=\"pace\">({{ split | swimmingPace: race.splits[i].distance }})</span>\n                                    <span *ngIf=\"race.splits[i].sport == 'cycling'\" class=\"pace\">({{ split | kmh: race.splits[i].distance }})</span>\n                                </td>\n                            </ng-container>\n                            <td>{{ result.total | secondsToTime }}</td>\n                        </ng-container>\n                    </tr>\n                </ng-container>\n            </tbody>\n        </table>\n    </ng-container>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboards/ranking/multisport-ranking-table/multisport-ranking-table.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#ranking {\n  width: 100%;\n  max-width: 100%; }\n  #ranking .table thead {\n    color: #fff; }\n  #ranking .table th {\n    width: auto; }\n  #ranking .table td {\n    width: auto; }\n  #ranking .table dl {\n    margin: 1rem 0; }\n    #ranking .table dl dd {\n      border-bottom: 1px solid #ccc;\n      padding-left: 1rem; }\n    #ranking .table dl dd.last {\n      border-bottom: none; }\n  #ranking .pace {\n    font-style: italic; }\n\nspan.total {\n  display: block;\n  font-style: italic; }\n\n.input-group .input-group-btn > .btn {\n  border: 1px solid #ced4da;\n  background-color: #F7F7F9 !important; }\n  .input-group .input-group-btn > .btn:focus {\n    color: #55595c;\n    background-color: #f7f7f9;\n    border-color: #ced4da;\n    outline: none;\n    box-shadow: 0 0 0 0; }\n\n.input-group input:focus {\n  color: #55595c;\n  background-color: #f7f7f9;\n  border-color: #ced4da;\n  outline: none;\n  box-shadow: 0 0 0 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/ranking/multisport-ranking-table/multisport-ranking-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MultisportRankingTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_race_model__ = __webpack_require__("../../../../../src/app/model/race.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MultisportRankingTableComponent = (function () {
    function MultisportRankingTableComponent(router) {
        this.router = router;
        this.filterRanking = false;
    }
    MultisportRankingTableComponent.prototype.ngOnInit = function () {
        this.onWindowScroll();
    };
    MultisportRankingTableComponent.prototype.goToDetail = function (id) {
        this.router.navigate([this.race.edition + '/athlete/' + id]);
    };
    MultisportRankingTableComponent.prototype.showDetail = function (id) {
        if (this.detail != id) {
            this.detail = id;
        }
        else {
            this.detail = "0";
        }
    };
    MultisportRankingTableComponent.prototype.updateRanking = function () {
        if (this.search.length > 2) {
            this.filterRanking = true;
        }
        else {
            this.filterRanking = false;
        }
    };
    MultisportRankingTableComponent.prototype.onWindowScroll = function () {
        if (window.innerWidth < 768) {
            this.isMobile = true;
        }
        else {
            this.isMobile = false;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], MultisportRankingTableComponent.prototype, "ranking", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__model_race_model__["a" /* Race */])
    ], MultisportRankingTableComponent.prototype, "race", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], MultisportRankingTableComponent.prototype, "navigateToDetail", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], MultisportRankingTableComponent.prototype, "showSearch", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], MultisportRankingTableComponent.prototype, "isLoading", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], MultisportRankingTableComponent.prototype, "order", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])("window:resize", []),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], MultisportRankingTableComponent.prototype, "onWindowScroll", null);
    MultisportRankingTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'multisport-ranking-table',
            template: __webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking-table/multisport-ranking-table.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking-table/multisport-ranking-table.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], MultisportRankingTableComponent);
    return MultisportRankingTableComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid pt-4\">\n    <h2>{{ 'dashboard.titles.ranking' | translate }}</h2>\n</div>\n\n<race-cards [athletes] = \"race.metadata.athletes\"\n            [men] = \"race.metadata.men\"\n            [women] = \"race.metadata.women\"\n            [distance] = \"race.metadata.distance\"\n>\n</race-cards>\n\n<div class=\"container-fluid pt-4\">\n    <p>\n        <a class=\"pull-right\" (click)=\"toogleFilters()\">\n            <span *ngIf=\"showFilters\">\n                <i class=\"fa fa-angle-up\" aria-hidden=\"true\"></i>\n                {{ 'dashboard.filters.hide-filters' | translate }}\n            </span>\n            <span *ngIf=\"!showFilters\">\n                <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                {{ 'dashboard.filters.show-filters' | translate }}\n            </span>\n        </a>\n    </p>\n</div>\n\n<div id=\"ranking-filters\" class =\"container-fluid pt-4 flex-row align-items-center\" [class.hide-filters]=\"!showFilters\" [class.show-filters]=\"showFilters\">\n    <form #filterForm=\"ngForm\">\n        <div class=\"row\">\n            <div class=\"col-lg-6 col-md-6 col-xs-12\">\n                <div class=\"form-group euclides-background-color-1\">\n                    <fieldset class=\"p-4\">\n                        <label for=\"groupFilter\">{{ 'dashboard.filters.by-group' | translate }}</label>\n                        <select [disabled]=\"isLoading\" class=\"form-control\" id=\"groupFilter\" name=\"rankingGroup\" [(ngModel)]=\"rankingGroup\" (change)=\"updateRanking()\">\n                            <option value=\"global\"> {{ 'dashboard.filters.global' | translate }} </option>\n                            <option *ngFor=\"let group of race.metadata.groups\" value=\"{{ group }}\"> {{ 'dashboard.filters.' + group | translate }}</option>\n                        </select>\n                    </fieldset>\n                </div>\n            </div>\n            <div class=\"col-lg-6 col-md-6 col-xs-12\">\n                <div class=\"form-group euclides-background-color-1\">\n                    <fieldset class=\"p-4\">\n                        <label for=\"categoryFilter\">{{ 'dashboard.filters.by-category' | translate }}</label>\n                        <select [disabled]=\"isLoading\" class=\"form-control\" id=\"categoryFilter\" name=\"rankingCategory\" [(ngModel)]=\"rankingCategory\" (change)=\"updateRanking()\">\n                            <option value=\"all-categories\"> {{ 'dashboard.filters.all-categories' | translate }} </option>\n                            <option *ngFor=\"let category of race.metadata.categories\" value=\"{{ category }}\"> {{ category }} </option>\n                        </select>\n                    </fieldset>\n                </div>\n            </div>\n            <div class=\"col-lg-6 col-md-6 col-xs-12\">\n                    <div class=\"form-group euclides-background-color-1\">\n                        <fieldset class=\"p-4\">\n                            <label for=\"teamFilter\">{{ 'dashboard.filters.by-team' | translate }}</label>\n                            <select [disabled]=\"isLoading\" class=\"form-control\" id=\"teamFilter\" name=\"rankingTeam\" [(ngModel)]=\"rankingTeam\" (change)=\"updateRanking()\">\n                                <option value=\"all-teams\"> {{ 'dashboard.filters.all-teams' | translate }} </option>\n                                <option *ngFor=\"let team of race.metadata.teams\" value=\"{{ team.url }}\"> {{ team.name }} </option>\n                            </select>\n                        </fieldset>\n                    </div>\n                </div>\n            <div class=\"col-lg-6 col-md-6 col-xs-12\">\n                <div class=\"form-group euclides-background-color-1\">\n                    <fieldset class=\"p-4\">\n                        <label for=\"disciplineFilter\">{{ 'dashboard.filters.by-discipline' | translate }}</label>\n                        <select [disabled]=\"isLoading\" class=\"form-control\" id=\"disciplineFilter\" name=\"rankingDiscipline\" [(ngModel)]=\"rankingDiscipline\" (change)=\"updateRanking()\">\n                            <option value=\"-1\"> {{ 'dashboard.filters.total' | translate }} </option>\n                            <ng-container *ngFor=\"let split of race.splits; let i = index;\">\n                                <option *ngIf=\"race.timeOnTransitions || split.sport !='transition'\" value=\"{{ i }}\"> \n                                    {{ split.sport | translate }}\n                                    <span *ngIf=\"split.loop > 0\">{{ split.loop }}</span>\n                                </option>\n                            </ng-container>\n                        </select>\n                    </fieldset>\n                </div>\n            </div>\n        </div>\n    </form>\n</div>\n\n<div *ngIf=\"error && error != 404\" id=\"error-box\" class=\"flex-row align-items-center euclides-background-color-2\">\n    <div class=\"container-fluid\">\n        <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>                  \n    </div>\n</div>\n\n<multisport-ranking-table *ngIf=\"!error || error == 404\" \n    [race]=\"race\" \n    [ranking]=\"ranking\" \n    [isLoading]=\"isLoading\"\n    [showSearch]=\"true\"\n    [navigateToDetail]=\"true\" \n    [order]=\"rankingDiscipline\">\n</multisport-ranking-table>"

/***/ }),

/***/ "../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#ranking-filters {\n  overflow: hidden; }\n  #ranking-filters .form-control {\n    padding: 0.75rem .5rem; }\n  #ranking-filters.hide-filters {\n    position: absolute;\n    opacity: 0;\n    visibility: hidden;\n    top: -1000px; }\n  #ranking-filters.show-filters {\n    opacity: 1;\n    visibility: visible;\n    transition: opacity 600ms, visibility 600ms; }\n\nh2 {\n  font-size: 1.2rem; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MultisportRankingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ALL_GROUPS = "global";
var ALL_CATEGORIES = "all-categories";
var ALL_TEAMS = "all-teams";
var ALL_SPLITS = -1;
var MultisportRankingComponent = (function () {
    function MultisportRankingComponent(router, rankingService) {
        this.router = router;
        this.rankingService = rankingService;
        this.rankingGroup = ALL_GROUPS;
        this.rankingCategory = ALL_CATEGORIES;
        this.rankingDiscipline = ALL_SPLITS;
        this.rankingTeam = ALL_TEAMS;
        this.isLoading = true;
        this.showFilters = true;
    }
    MultisportRankingComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey)) {
            this.race = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey));
            this.updateRanking();
        }
    };
    MultisportRankingComponent.prototype.updateRanking = function () {
        this.isLoading = true;
        this.getRanking(this.race.edition, this.rankingGroup, this.rankingCategory, this.rankingTeam, this.rankingDiscipline);
    };
    MultisportRankingComponent.prototype.getRanking = function (edition, group, category, team, split) {
        var _this = this;
        this.rankingService.getRankingByEdition(edition, group, category, team, split)
            .subscribe(function (ranking) {
            _this.ranking = ranking;
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.ranking = new Array();
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    MultisportRankingComponent.prototype.toogleFilters = function () {
        this.showFilters = !this.showFilters;
    };
    MultisportRankingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'multisport-ranking',
            template: __webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__["a" /* RankingService */]],
            exportAs: 'triathlon-ranking, duathlon-ranking'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__["a" /* RankingService */]])
    ], MultisportRankingComponent);
    return MultisportRankingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/routing/app-route.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRouteModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_component__ = __webpack_require__("../../../../../src/app/dashboards/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ranking_multisport_ranking_multisport_ranking_component__ = __webpack_require__("../../../../../src/app/dashboards/ranking/multisport-ranking/multisport-ranking.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__athlete_multisport_athlete_multisport_athlete_component__ = __webpack_require__("../../../../../src/app/dashboards/athlete/multisport-athlete/multisport-athlete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__team_multisport_team_multisport_team_component__ = __webpack_require__("../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: ':id',
        component: __WEBPACK_IMPORTED_MODULE_2__dashboard_component__["a" /* DashboardComponent */],
        children: [
            { path: '', redirectTo: 'ranking', pathMatch: 'full' },
            { path: 'ranking', component: __WEBPACK_IMPORTED_MODULE_3__ranking_multisport_ranking_multisport_ranking_component__["a" /* MultisportRankingComponent */] },
            { path: 'athlete', component: __WEBPACK_IMPORTED_MODULE_4__athlete_multisport_athlete_multisport_athlete_component__["a" /* MultisportAthleteComponent */] },
            { path: 'athlete/:athlete', component: __WEBPACK_IMPORTED_MODULE_4__athlete_multisport_athlete_multisport_athlete_component__["a" /* MultisportAthleteComponent */] },
            { path: 'teams', component: __WEBPACK_IMPORTED_MODULE_5__team_multisport_team_multisport_team_component__["a" /* MultisportTeamComponent */] },
            { path: '**', component: __WEBPACK_IMPORTED_MODULE_3__ranking_multisport_ranking_multisport_ranking_component__["a" /* MultisportRankingComponent */] }
        ]
    }
];
var AppRouteModule = (function () {
    function AppRouteModule() {
    }
    AppRouteModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], AppRouteModule);
    return AppRouteModule;
}());



/***/ }),

/***/ "../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid pt-4\">\n    <h2>{{ 'dashboard.titles.ranking-by-team' | translate }}</h2>\n</div>\n\n<div *ngIf=\"error\" id=\"error-box\" class=\"flex-row align-items-center euclides-background-color-2\">\n    <div class=\"container-fluid\">\n        <p class=\"alert alert-dismissible alert-danger\">{{ 'dashboard.errors.generic' | translate }} </p>                  \n    </div>\n</div>\n\n<ng-container *ngIf=\"!error\">\n  <team-cards [athletes] = \"race.metadata.athletes\"\n              [teams] = \"race.metadata.numberOfTeams\"\n              [notTeam] = \"race.metadata.notTeam\"\n              [notFederated] = \"race.metadata.notFederated\"\n  >\n  </team-cards>\n\n  <div id=\"ranking\" class=\"container-fluid pt-4\">\n    <div class=\"loading\" *ngIf=\"isLoading\">\n      <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\n      <span class=\"sr-only\">Loading...</span>\n    </div>\n\n    <ng-container *ngIf=\"!isLoading\">\n        <div *ngIf=\"ranking && ranking.length == 0\" class=\"alert alert-danger\">\n          <strong>{{ 'dashboard.errors.no-results' | translate }}</strong>\n        </div>\n\n        <table class=\"table table-hover table-bordered table-sm euclides-background-color-1\">\n          <thead class=\"euclides-background-color-6\">\n            <tr>\n              <th></th>\n              <th>{{ 'club' | translate }}</th>\n                <ng-container *ngIf=\"!isMobile\">\n                  <ng-container *ngFor=\"let split of race.splits\">\n                    <th *ngIf=\"split.sport != 'transition' || race.timeOnTransitions\">\n                      {{ split.sport | translate }}\n                      <span *ngIf=\"split.loop > 0\">{{ split.loop }}</span>\n                    </th>\n                  </ng-container>\n                </ng-container>\n              <th>{{ 'total' | translate }}</th>\n            </tr>\n          </thead>\n\n          <tbody>\n            <ng-container *ngFor=\"let teamResult of ranking; let i = index\">\n              <tr class=\"table-info team\">\n                <td>\n                    <span>{{ i + 1 }}</span>\n                </td>\n                <td *ngIf=\"!isMobile\" [attr.colspan]=\"columns + 1\" >{{ teamResult.team.name }}</td>\n                <td *ngIf=\"isMobile\">{{ teamResult.team.name }}</td>\n                <td> {{ teamResult.total | secondsToTime }} </td>\n              </tr>\n              <tr *ngFor=\"let result of teamResult.results; let x = index\" (click) = \"goToDetail(result._id)\" class=\"actionable\">\n                <td></td>\n                <td class=\"athlete\">{{ result.name }}</td>\n                <ng-container *ngIf=\"!isMobile\">\n                  <ng-container *ngFor=\"let split of result.splits; let i = index\">\n                    <td *ngIf=\"race.splits[i].sport != 'transition' || race.timeOnTransitions\">\n                      {{ split | secondsToTime }}\n                      <span *ngIf=\"race.splits[i].sport == 'swimming'\" class=\"pace\">({{ split | swimmingPace: race.splits[i].distance }})</span>\n                      <span *ngIf=\"race.splits[i].sport == 'running'\" class=\"pace\">({{ split | pace: race.splits[i].distance }})</span>\n                      <span *ngIf=\"race.splits[i].sport == 'cycling'\" class=\"pace\">({{ split | kmh: race.splits[i].distance }})</span>\n                    </td>\n                  </ng-container>\n                </ng-container>\n                <td>{{ result.total | secondsToTime }}</td>\n              </tr>\n            </ng-container>\n          </tbody>\n        </table>\n    </ng-container>\n  </div>\n  \n  <div class=\"container-fluid pt-4\">\n    <div class=\"col-xs-12\">\n      <div class=\"chart pt-4 pb-4 mb-4 euclides-background-color-1\">\n          <athletes-by-team [edition]=\"race.edition\"></athletes-by-team>\n      </div>\n    </div>\n  </div>\n\n</ng-container>\n\n"

/***/ }),

/***/ "../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#ranking {\n  width: 100%;\n  max-width: 100%; }\n  #ranking .table thead {\n    color: #fff; }\n  #ranking .table th {\n    width: auto; }\n  #ranking .table td {\n    width: auto; }\n    #ranking .table td.athlete {\n      padding-left: 1rem !important; }\n  #ranking .table tr.team {\n    color: #000; }\n  #ranking .table dl {\n    margin: 1rem 0; }\n    #ranking .table dl dd {\n      border-bottom: 1px solid #ccc;\n      padding-left: 1rem; }\n    #ranking .table dl dd.last {\n      border-bottom: none; }\n  #ranking .pace {\n    font-style: italic; }\n\nspan.total {\n  display: block;\n  font-style: italic; }\n\nh2 {\n  font-size: 1.2rem; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MultisportTeamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__ = __webpack_require__("../../../../../src/app/service/ranking/ranking.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DEFAULT_GROUP = "male";
var MultisportTeamComponent = (function () {
    function MultisportTeamComponent(rankingService, router) {
        this.rankingService = rankingService;
        this.router = router;
        this.isLoading = true;
        this.group = DEFAULT_GROUP;
    }
    MultisportTeamComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey)) {
            this.race = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].racesKey));
            this.onWindowScroll();
            this.calculateColumns();
            this.getRanking(this.group);
        }
    };
    MultisportTeamComponent.prototype.getRanking = function (group) {
        var _this = this;
        this.rankingService.getTeamRankingByEdition(this.race.edition, group, __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].teamRankingNOfAthletes)
            .subscribe(function (ranking) {
            _this.ranking = ranking;
            _this.error = undefined;
            _this.isLoading = false;
        }, function (error) {
            _this.error = error.status > 0 ? error.status : 404;
            _this.isLoading = false;
        });
    };
    MultisportTeamComponent.prototype.calculateColumns = function () {
        this.columns = this.race.timeOnTransitions ? this.race.splits.length :
            this.race.splits.filter(function (split) { return split.sport != 'transition'; }).length;
    };
    MultisportTeamComponent.prototype.goToDetail = function (id) {
        this.router.navigate([this.race.edition + '/athlete/' + id]);
    };
    MultisportTeamComponent.prototype.onWindowScroll = function () {
        if (window.innerWidth < 768) {
            this.isMobile = true;
        }
        else {
            this.isMobile = false;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])("window:resize", []),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], MultisportTeamComponent.prototype, "onWindowScroll", null);
    MultisportTeamComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-multisport-team',
            template: __webpack_require__("../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboards/team/multisport-team/multisport-team.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__["a" /* RankingService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__service_ranking_ranking_service__["a" /* RankingService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], MultisportTeamComponent);
    return MultisportTeamComponent;
}());



/***/ }),

/***/ "../../../../../src/app/model/item.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Item; });
var Item = (function () {
    function Item(id, text) {
        this.id = id;
        this.text = text;
    }
    return Item;
}());



/***/ }),

/***/ "../../../../../src/app/model/race-metadata.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RaceMetadata; });
var RaceMetadata = (function () {
    function RaceMetadata(men, women, athletes, notFederated, notTeam, numberOfTeams, distance, groups, categories, teams) {
        this.men = men;
        this.women = women;
        this.athletes = athletes;
        this.notFederated = notFederated;
        this.notTeam = notTeam;
        this.numberOfTeams = numberOfTeams;
        this.distance = distance;
        this.groups = groups;
        this.categories = categories;
        this.teams = teams;
    }
    return RaceMetadata;
}());



/***/ }),

/***/ "../../../../../src/app/model/race.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Race; });
var Race = (function () {
    function Race(_id, date, edition, name, race, year, type, timestamp, sport, format, splits, location, timeOnTransitions, metadata) {
        this._id = _id;
        this.date = date;
        this.edition = edition;
        this.name = name;
        this.race = race;
        this.year = year;
        this.type = type;
        this.timestamp = timestamp;
        this.sport = sport;
        this.format = format;
        this.splits = splits;
        this.location = location;
        this.timeOnTransitions = timeOnTransitions;
        this.metadata = metadata;
    }
    return Race;
}());



/***/ }),

/***/ "../../../../../src/app/model/result.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Result; });
var Result = (function () {
    function Result(_id, date, splits, race, edition, year, timestamp, name, name_normalized, club, ranking, state, category, sport, group, format, type, number, total, team) {
        this._id = _id;
        this.date = date;
        this.splits = splits;
        this.race = race;
        this.edition = edition;
        this.year = year;
        this.timestamp = timestamp;
        this.name = name;
        this.name_normalized = name_normalized;
        this.club = club;
        this.ranking = ranking;
        this.state = state;
        this.category = category;
        this.sport = sport;
        this.group = group;
        this.format = format;
        this.type = type;
        this.number = number;
        this.total = total;
        this.team = team;
    }
    return Result;
}());



/***/ }),

/***/ "../../../../../src/app/race-selector/race-selector.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"race-selector\" class=\"flex-row align-items-center euclides-background-color-2\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-lg-8\">\n          <div class=\"card p-4 euclides-background-color-1 euclides-border-color-6\">\n            <div class=\"card-body\">\n              <h1>{{ 'race-selector.welcome' | translate }}</h1>\n                <ng-select \n                  [items]=\"items\"\n                  (data)=\"refreshValue($event)\"\n                  placeholder= \"{{ 'race-selector.choose-race' | translate }}\">\n                </ng-select>\n\n                <p class=\"text-danger pull-right clearfix\" *ngIf=\"noRaceSelectedError\">\n                  {{ 'race-selector.errors.no-race-selected' | translate }}\n                </p>\n\n                <p class=\"pull-right clearfix\">\n                  <button class=\"btn btn-info actionable\" (click)=\"goToRace()\">\n                    {{ 'race-selector.continue' | translate }}\n                  </button>\n                </p>\n            </div>\n          </div>\n          <div class=\"card text-white euclides-background-color-6 py-5 d-md-down-none euclides-border-color-6\">\n            <div class=\"card-body text-center\">\n              <div>\n                <p [innerHTML] = \"'race-selector.how-to' | translate\"></p>\n              </div>\n            </div>\n          </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/race-selector/race-selector.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div#race-selector {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/race-selector/race-selector.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RaceSelectorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_race_race_service__ = __webpack_require__("../../../../../src/app/service/race/race.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_item_model__ = __webpack_require__("../../../../../src/app/model/item.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RaceSelectorComponent = (function () {
    function RaceSelectorComponent(raceService, router) {
        this.raceService = raceService;
        this.router = router;
        this.items = new Array();
        this.noRaceSelectedError = false;
    }
    RaceSelectorComponent.prototype.ngOnInit = function () {
        this.getItems();
        localStorage.clear();
    };
    RaceSelectorComponent.prototype.getItems = function () {
        var _this = this;
        this.raceService.getRaces().subscribe(function (races) {
            _this.items = _this.fromRacesToItems(races);
        });
    };
    RaceSelectorComponent.prototype.refreshValue = function (value) {
        this.item = value;
        this.noRaceSelectedError = false;
    };
    RaceSelectorComponent.prototype.goToRace = function () {
        if (this.item != undefined && this.item != null) {
            this.noRaceSelectedError = false;
            this.router.navigate([this.item.id]);
        }
        else {
            this.noRaceSelectedError = true;
        }
    };
    RaceSelectorComponent.prototype.fromRacesToItems = function (races) {
        var items = new Array();
        for (var _i = 0, races_1 = races; _i < races_1.length; _i++) {
            var race = races_1[_i];
            var item = new __WEBPACK_IMPORTED_MODULE_3__model_item_model__["a" /* Item */](race.edition, race.name);
            items.push(item);
        }
        return items;
    };
    RaceSelectorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'race-selector',
            template: __webpack_require__("../../../../../src/app/race-selector/race-selector.component.html"),
            styles: [__webpack_require__("../../../../../src/app/race-selector/race-selector.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__service_race_race_service__["a" /* RaceService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_race_race_service__["a" /* RaceService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], RaceSelectorComponent);
    return RaceSelectorComponent;
}());



/***/ }),

/***/ "../../../../../src/app/routing/app-route.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRouteModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__race_selector_race_selector_component__ = __webpack_require__("../../../../../src/app/race-selector/race-selector.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__race_selector_race_selector_component__["a" /* RaceSelectorComponent */] }
];
var AppRouteModule = (function () {
    function AppRouteModule() {
    }
    AppRouteModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forRoot(routes)
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], AppRouteModule);
    return AppRouteModule;
}());



/***/ }),

/***/ "../../../../../src/app/service/api-endpoints.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return API_ENDPOINTS; });
var API_ENDPOINTS = {
    // Common
    HEALTH_ENDPOINT: "health/",
    // Races
    GET_RACES_ENDPOINT: "races/",
    GET_RACE_ENDPOINT: "race/",
    // Results
    GET_ALL_RESULTS_ENDPOINT: "ranking/",
    GET_RESULT: "result/",
    GET_TEAM_AND_CATEGORIES_BY_EDITION: "results/teams-and-categories/",
    GET_AVERAGE_BY_EDITION: "results/average/",
    COUNT_BY_TEAM_BY_EDITION: "results/count-by-team/",
    GET_RANKING_BY_EDITION: "ranking/",
    GET_RESULT_HISTORY: "results/history/",
    GET_ATHLETE_RANKING: "athletes/ranking/",
    GET_ATHLETES: "athletes/",
    GET_TEAM_RANKING_BY_EDITION: "team/ranking/"
};


/***/ }),

/***/ "../../../../../src/app/service/race/race.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RaceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_endpoints__ = __webpack_require__("../../../../../src/app/service/api-endpoints.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RaceService = (function () {
    function RaceService(http) {
        this.http = http;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiUrl;
    }
    ;
    RaceService.prototype.getRaces = function () {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_RACES_ENDPOINT);
    };
    RaceService.prototype.getRaceByEdition = function (editionId) {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_RACE_ENDPOINT + editionId);
    };
    RaceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], RaceService);
    return RaceService;
}());



/***/ }),

/***/ "../../../../../src/app/service/ranking/ranking.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RankingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_endpoints__ = __webpack_require__("../../../../../src/app/service/api-endpoints.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RankingService = (function () {
    function RankingService(http) {
        this.http = http;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiUrl;
    }
    ;
    RankingService.prototype.getRanking = function () {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_ALL_RESULTS_ENDPOINT);
    };
    RankingService.prototype.getResult = function (resultId) {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_RESULT + resultId);
    };
    RankingService.prototype.getTeamsAndCategories = function (editionId) {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_TEAM_AND_CATEGORIES_BY_EDITION + editionId);
    };
    RankingService.prototype.countByTeam = function (editionId) {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].COUNT_BY_TEAM_BY_EDITION + editionId);
    };
    RankingService.prototype.getRankingByEdition = function (editionId, group, category, team, split, athletes) {
        if (athletes === void 0) { athletes = "all-athletes"; }
        var url = this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_RANKING_BY_EDITION + editionId +
            "?group=" + group +
            "&category=" + category +
            "&team=" + team +
            "&split=" + split +
            "&athletes=" + athletes;
        return this.http.get(url);
    };
    RankingService.prototype.getResultHistory = function (resultId) {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_RESULT_HISTORY + resultId);
    };
    RankingService.prototype.getAthleteRanking = function (athleteId) {
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_ATHLETE_RANKING + athleteId);
    };
    RankingService.prototype.getAverage = function (editionId, filter, value) {
        if (filter === void 0) { filter = null; }
        if (value === void 0) { value = null; }
        var condition = "";
        if (filter != undefined && filter != null && value != undefined && value != null) {
            condition = "?" + filter + "=" + value;
        }
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_AVERAGE_BY_EDITION + editionId + condition);
    };
    RankingService.prototype.getAthletesByEdition = function (editionId, team) {
        if (team === void 0) { team = "all-teams"; }
        return this.http.get(this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_ATHLETES + editionId + "?team=" + team);
    };
    RankingService.prototype.getTeamRankingByEdition = function (editionId, group, numberOfAtheletes) {
        var url = this.apiUrl + __WEBPACK_IMPORTED_MODULE_3__api_endpoints__["a" /* API_ENDPOINTS */].GET_TEAM_RANKING_BY_EDITION + editionId +
            "?group=" + group +
            "&numberOfAthletes=" + numberOfAtheletes;
        return this.http.get(url);
    };
    RankingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], RankingService);
    return RankingService;
}());



/***/ }),

/***/ "../../../../../src/app/shared/modules/translate-shared-module/translate-shared-module.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpLoaderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TranslateSharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_http_loader__ = __webpack_require__("../../../../@ngx-translate/http-loader/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





function HttpLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_4__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var TranslateSharedModule = (function () {
    function TranslateSharedModule() {
    }
    TranslateSharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: HttpLoaderFactory,
                        deps: [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]]
                    }
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
            declarations: []
        })
    ], TranslateSharedModule);
    return TranslateSharedModule;
}());



/***/ }),

/***/ "../../../../../src/app/shared/pipes/kmh.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KmhPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KmhPipe = (function () {
    function KmhPipe() {
    }
    KmhPipe.prototype.transform = function (seconds, distance) {
        if (seconds == 0)
            return "0.0";
        return String(Math.round(distance / 1000 / (seconds / 60 / 60) * 100) / 100);
    };
    KmhPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'kmh'
        })
    ], KmhPipe);
    return KmhPipe;
}());



/***/ }),

/***/ "../../../../../src/app/shared/pipes/pace.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PacePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PacePipe = (function () {
    function PacePipe(timeConvertionUtil) {
        this.timeConvertionUtil = timeConvertionUtil;
    }
    PacePipe.prototype.transform = function (seconds, distance) {
        return this.timeConvertionUtil.timeToPace(seconds, distance);
    };
    PacePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'pace'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_time_conversion_utils__["a" /* TimeConversionUtil */]])
    ], PacePipe);
    return PacePipe;
}());



/***/ }),

/***/ "../../../../../src/app/shared/pipes/seconds-to-time.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecondsToTimePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SecondsToTimePipe = (function () {
    function SecondsToTimePipe(timeConversionUtil) {
        this.timeConversionUtil = timeConversionUtil;
    }
    SecondsToTimePipe.prototype.transform = function (seconds) {
        return this.timeConversionUtil.secondsToTime(seconds);
    };
    SecondsToTimePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'secondsToTime'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_time_conversion_utils__["a" /* TimeConversionUtil */]])
    ], SecondsToTimePipe);
    return SecondsToTimePipe;
}());



/***/ }),

/***/ "../../../../../src/app/shared/pipes/swimming-pace.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SwimmingPacePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_time_conversion_utils__ = __webpack_require__("../../../../../src/app/shared/utils/time-conversion.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SwimmingPacePipe = (function () {
    function SwimmingPacePipe(timeConversionUtil) {
        this.timeConversionUtil = timeConversionUtil;
    }
    SwimmingPacePipe.prototype.transform = function (seconds, distance) {
        return this.timeConversionUtil.timeToSwimmingPace(seconds, distance);
    };
    SwimmingPacePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'swimmingPace'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_time_conversion_utils__["a" /* TimeConversionUtil */]])
    ], SwimmingPacePipe);
    return SwimmingPacePipe;
}());



/***/ }),

/***/ "../../../../../src/app/shared/utils/time-conversion.utils.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeConversionUtil; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment__);

var TimeConversionUtil = (function () {
    function TimeConversionUtil() {
    }
    TimeConversionUtil.prototype.secondsToTime = function (seconds) {
        return this.format(seconds);
    };
    TimeConversionUtil.prototype.timePercentage = function (split, total) {
        if (split == 0 || total == 0)
            return "0%";
        return Math.round(((split / total) * 100)) + "%";
    };
    TimeConversionUtil.prototype.timeToPace = function (seconds, distance) {
        if (seconds == 0)
            return "0:00";
        return this.format(this.timeToPaceInSeconds(seconds, distance), false);
    };
    TimeConversionUtil.prototype.timeToSwimmingPace = function (seconds, distance) {
        if (seconds == 0)
            return "0:00";
        return this.format(this.timeToPaceInSeconds(seconds, distance, 100), false);
    };
    TimeConversionUtil.prototype.timeToPaceInSeconds = function (seconds, distance, pace) {
        if (pace === void 0) { pace = 1000; }
        if (seconds == 0)
            return 0;
        distance = distance / pace;
        return seconds / distance;
    };
    TimeConversionUtil.prototype.differenceInPercentage = function (seconds1, seconds2) {
        if (seconds1 > seconds2)
            return (100 - ((seconds2 * 100) / seconds1)).toFixed(2) + "%";
        else
            return (100 - ((seconds1 * 100) / seconds2)).toFixed(2) + "%";
    };
    TimeConversionUtil.prototype.format = function (timeInSeconds, includeHoursIfEmpty) {
        if (includeHoursIfEmpty === void 0) { includeHoursIfEmpty = true; }
        var format = "HH:mm:ss";
        if (!includeHoursIfEmpty && timeInSeconds < 3600) {
            format = "mm:ss";
        }
        return __WEBPACK_IMPORTED_MODULE_0_moment__().startOf('day')
            .seconds(timeInSeconds)
            .format(format);
    };
    return TimeConversionUtil;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    apiUrl: '/api/',
    racesKey: 'race',
    resultKey: 'result',
    athleteKey: 'athlete',
    notTeam: ['INDEPENDIENTE'],
    notFederated: ['SEGURO-1-DIA', 'NO-FEDERADO'],
    teamRankingNOfAthletes: 3
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ "../../../../chart.js/node_modules/moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../chart.js/node_modules/moment/locale/af.js",
	"./af.js": "../../../../chart.js/node_modules/moment/locale/af.js",
	"./ar": "../../../../chart.js/node_modules/moment/locale/ar.js",
	"./ar-dz": "../../../../chart.js/node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../chart.js/node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "../../../../chart.js/node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../chart.js/node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "../../../../chart.js/node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../chart.js/node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "../../../../chart.js/node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../chart.js/node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "../../../../chart.js/node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../chart.js/node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "../../../../chart.js/node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../chart.js/node_modules/moment/locale/ar-tn.js",
	"./ar.js": "../../../../chart.js/node_modules/moment/locale/ar.js",
	"./az": "../../../../chart.js/node_modules/moment/locale/az.js",
	"./az.js": "../../../../chart.js/node_modules/moment/locale/az.js",
	"./be": "../../../../chart.js/node_modules/moment/locale/be.js",
	"./be.js": "../../../../chart.js/node_modules/moment/locale/be.js",
	"./bg": "../../../../chart.js/node_modules/moment/locale/bg.js",
	"./bg.js": "../../../../chart.js/node_modules/moment/locale/bg.js",
	"./bn": "../../../../chart.js/node_modules/moment/locale/bn.js",
	"./bn.js": "../../../../chart.js/node_modules/moment/locale/bn.js",
	"./bo": "../../../../chart.js/node_modules/moment/locale/bo.js",
	"./bo.js": "../../../../chart.js/node_modules/moment/locale/bo.js",
	"./br": "../../../../chart.js/node_modules/moment/locale/br.js",
	"./br.js": "../../../../chart.js/node_modules/moment/locale/br.js",
	"./bs": "../../../../chart.js/node_modules/moment/locale/bs.js",
	"./bs.js": "../../../../chart.js/node_modules/moment/locale/bs.js",
	"./ca": "../../../../chart.js/node_modules/moment/locale/ca.js",
	"./ca.js": "../../../../chart.js/node_modules/moment/locale/ca.js",
	"./cs": "../../../../chart.js/node_modules/moment/locale/cs.js",
	"./cs.js": "../../../../chart.js/node_modules/moment/locale/cs.js",
	"./cv": "../../../../chart.js/node_modules/moment/locale/cv.js",
	"./cv.js": "../../../../chart.js/node_modules/moment/locale/cv.js",
	"./cy": "../../../../chart.js/node_modules/moment/locale/cy.js",
	"./cy.js": "../../../../chart.js/node_modules/moment/locale/cy.js",
	"./da": "../../../../chart.js/node_modules/moment/locale/da.js",
	"./da.js": "../../../../chart.js/node_modules/moment/locale/da.js",
	"./de": "../../../../chart.js/node_modules/moment/locale/de.js",
	"./de-at": "../../../../chart.js/node_modules/moment/locale/de-at.js",
	"./de-at.js": "../../../../chart.js/node_modules/moment/locale/de-at.js",
	"./de-ch": "../../../../chart.js/node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "../../../../chart.js/node_modules/moment/locale/de-ch.js",
	"./de.js": "../../../../chart.js/node_modules/moment/locale/de.js",
	"./dv": "../../../../chart.js/node_modules/moment/locale/dv.js",
	"./dv.js": "../../../../chart.js/node_modules/moment/locale/dv.js",
	"./el": "../../../../chart.js/node_modules/moment/locale/el.js",
	"./el.js": "../../../../chart.js/node_modules/moment/locale/el.js",
	"./en-au": "../../../../chart.js/node_modules/moment/locale/en-au.js",
	"./en-au.js": "../../../../chart.js/node_modules/moment/locale/en-au.js",
	"./en-ca": "../../../../chart.js/node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "../../../../chart.js/node_modules/moment/locale/en-ca.js",
	"./en-gb": "../../../../chart.js/node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "../../../../chart.js/node_modules/moment/locale/en-gb.js",
	"./en-ie": "../../../../chart.js/node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "../../../../chart.js/node_modules/moment/locale/en-ie.js",
	"./en-nz": "../../../../chart.js/node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "../../../../chart.js/node_modules/moment/locale/en-nz.js",
	"./eo": "../../../../chart.js/node_modules/moment/locale/eo.js",
	"./eo.js": "../../../../chart.js/node_modules/moment/locale/eo.js",
	"./es": "../../../../chart.js/node_modules/moment/locale/es.js",
	"./es-do": "../../../../chart.js/node_modules/moment/locale/es-do.js",
	"./es-do.js": "../../../../chart.js/node_modules/moment/locale/es-do.js",
	"./es.js": "../../../../chart.js/node_modules/moment/locale/es.js",
	"./et": "../../../../chart.js/node_modules/moment/locale/et.js",
	"./et.js": "../../../../chart.js/node_modules/moment/locale/et.js",
	"./eu": "../../../../chart.js/node_modules/moment/locale/eu.js",
	"./eu.js": "../../../../chart.js/node_modules/moment/locale/eu.js",
	"./fa": "../../../../chart.js/node_modules/moment/locale/fa.js",
	"./fa.js": "../../../../chart.js/node_modules/moment/locale/fa.js",
	"./fi": "../../../../chart.js/node_modules/moment/locale/fi.js",
	"./fi.js": "../../../../chart.js/node_modules/moment/locale/fi.js",
	"./fo": "../../../../chart.js/node_modules/moment/locale/fo.js",
	"./fo.js": "../../../../chart.js/node_modules/moment/locale/fo.js",
	"./fr": "../../../../chart.js/node_modules/moment/locale/fr.js",
	"./fr-ca": "../../../../chart.js/node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../chart.js/node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "../../../../chart.js/node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../chart.js/node_modules/moment/locale/fr-ch.js",
	"./fr.js": "../../../../chart.js/node_modules/moment/locale/fr.js",
	"./fy": "../../../../chart.js/node_modules/moment/locale/fy.js",
	"./fy.js": "../../../../chart.js/node_modules/moment/locale/fy.js",
	"./gd": "../../../../chart.js/node_modules/moment/locale/gd.js",
	"./gd.js": "../../../../chart.js/node_modules/moment/locale/gd.js",
	"./gl": "../../../../chart.js/node_modules/moment/locale/gl.js",
	"./gl.js": "../../../../chart.js/node_modules/moment/locale/gl.js",
	"./gom-latn": "../../../../chart.js/node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../chart.js/node_modules/moment/locale/gom-latn.js",
	"./he": "../../../../chart.js/node_modules/moment/locale/he.js",
	"./he.js": "../../../../chart.js/node_modules/moment/locale/he.js",
	"./hi": "../../../../chart.js/node_modules/moment/locale/hi.js",
	"./hi.js": "../../../../chart.js/node_modules/moment/locale/hi.js",
	"./hr": "../../../../chart.js/node_modules/moment/locale/hr.js",
	"./hr.js": "../../../../chart.js/node_modules/moment/locale/hr.js",
	"./hu": "../../../../chart.js/node_modules/moment/locale/hu.js",
	"./hu.js": "../../../../chart.js/node_modules/moment/locale/hu.js",
	"./hy-am": "../../../../chart.js/node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "../../../../chart.js/node_modules/moment/locale/hy-am.js",
	"./id": "../../../../chart.js/node_modules/moment/locale/id.js",
	"./id.js": "../../../../chart.js/node_modules/moment/locale/id.js",
	"./is": "../../../../chart.js/node_modules/moment/locale/is.js",
	"./is.js": "../../../../chart.js/node_modules/moment/locale/is.js",
	"./it": "../../../../chart.js/node_modules/moment/locale/it.js",
	"./it.js": "../../../../chart.js/node_modules/moment/locale/it.js",
	"./ja": "../../../../chart.js/node_modules/moment/locale/ja.js",
	"./ja.js": "../../../../chart.js/node_modules/moment/locale/ja.js",
	"./jv": "../../../../chart.js/node_modules/moment/locale/jv.js",
	"./jv.js": "../../../../chart.js/node_modules/moment/locale/jv.js",
	"./ka": "../../../../chart.js/node_modules/moment/locale/ka.js",
	"./ka.js": "../../../../chart.js/node_modules/moment/locale/ka.js",
	"./kk": "../../../../chart.js/node_modules/moment/locale/kk.js",
	"./kk.js": "../../../../chart.js/node_modules/moment/locale/kk.js",
	"./km": "../../../../chart.js/node_modules/moment/locale/km.js",
	"./km.js": "../../../../chart.js/node_modules/moment/locale/km.js",
	"./kn": "../../../../chart.js/node_modules/moment/locale/kn.js",
	"./kn.js": "../../../../chart.js/node_modules/moment/locale/kn.js",
	"./ko": "../../../../chart.js/node_modules/moment/locale/ko.js",
	"./ko.js": "../../../../chart.js/node_modules/moment/locale/ko.js",
	"./ky": "../../../../chart.js/node_modules/moment/locale/ky.js",
	"./ky.js": "../../../../chart.js/node_modules/moment/locale/ky.js",
	"./lb": "../../../../chart.js/node_modules/moment/locale/lb.js",
	"./lb.js": "../../../../chart.js/node_modules/moment/locale/lb.js",
	"./lo": "../../../../chart.js/node_modules/moment/locale/lo.js",
	"./lo.js": "../../../../chart.js/node_modules/moment/locale/lo.js",
	"./lt": "../../../../chart.js/node_modules/moment/locale/lt.js",
	"./lt.js": "../../../../chart.js/node_modules/moment/locale/lt.js",
	"./lv": "../../../../chart.js/node_modules/moment/locale/lv.js",
	"./lv.js": "../../../../chart.js/node_modules/moment/locale/lv.js",
	"./me": "../../../../chart.js/node_modules/moment/locale/me.js",
	"./me.js": "../../../../chart.js/node_modules/moment/locale/me.js",
	"./mi": "../../../../chart.js/node_modules/moment/locale/mi.js",
	"./mi.js": "../../../../chart.js/node_modules/moment/locale/mi.js",
	"./mk": "../../../../chart.js/node_modules/moment/locale/mk.js",
	"./mk.js": "../../../../chart.js/node_modules/moment/locale/mk.js",
	"./ml": "../../../../chart.js/node_modules/moment/locale/ml.js",
	"./ml.js": "../../../../chart.js/node_modules/moment/locale/ml.js",
	"./mr": "../../../../chart.js/node_modules/moment/locale/mr.js",
	"./mr.js": "../../../../chart.js/node_modules/moment/locale/mr.js",
	"./ms": "../../../../chart.js/node_modules/moment/locale/ms.js",
	"./ms-my": "../../../../chart.js/node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "../../../../chart.js/node_modules/moment/locale/ms-my.js",
	"./ms.js": "../../../../chart.js/node_modules/moment/locale/ms.js",
	"./my": "../../../../chart.js/node_modules/moment/locale/my.js",
	"./my.js": "../../../../chart.js/node_modules/moment/locale/my.js",
	"./nb": "../../../../chart.js/node_modules/moment/locale/nb.js",
	"./nb.js": "../../../../chart.js/node_modules/moment/locale/nb.js",
	"./ne": "../../../../chart.js/node_modules/moment/locale/ne.js",
	"./ne.js": "../../../../chart.js/node_modules/moment/locale/ne.js",
	"./nl": "../../../../chart.js/node_modules/moment/locale/nl.js",
	"./nl-be": "../../../../chart.js/node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "../../../../chart.js/node_modules/moment/locale/nl-be.js",
	"./nl.js": "../../../../chart.js/node_modules/moment/locale/nl.js",
	"./nn": "../../../../chart.js/node_modules/moment/locale/nn.js",
	"./nn.js": "../../../../chart.js/node_modules/moment/locale/nn.js",
	"./pa-in": "../../../../chart.js/node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "../../../../chart.js/node_modules/moment/locale/pa-in.js",
	"./pl": "../../../../chart.js/node_modules/moment/locale/pl.js",
	"./pl.js": "../../../../chart.js/node_modules/moment/locale/pl.js",
	"./pt": "../../../../chart.js/node_modules/moment/locale/pt.js",
	"./pt-br": "../../../../chart.js/node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "../../../../chart.js/node_modules/moment/locale/pt-br.js",
	"./pt.js": "../../../../chart.js/node_modules/moment/locale/pt.js",
	"./ro": "../../../../chart.js/node_modules/moment/locale/ro.js",
	"./ro.js": "../../../../chart.js/node_modules/moment/locale/ro.js",
	"./ru": "../../../../chart.js/node_modules/moment/locale/ru.js",
	"./ru.js": "../../../../chart.js/node_modules/moment/locale/ru.js",
	"./sd": "../../../../chart.js/node_modules/moment/locale/sd.js",
	"./sd.js": "../../../../chart.js/node_modules/moment/locale/sd.js",
	"./se": "../../../../chart.js/node_modules/moment/locale/se.js",
	"./se.js": "../../../../chart.js/node_modules/moment/locale/se.js",
	"./si": "../../../../chart.js/node_modules/moment/locale/si.js",
	"./si.js": "../../../../chart.js/node_modules/moment/locale/si.js",
	"./sk": "../../../../chart.js/node_modules/moment/locale/sk.js",
	"./sk.js": "../../../../chart.js/node_modules/moment/locale/sk.js",
	"./sl": "../../../../chart.js/node_modules/moment/locale/sl.js",
	"./sl.js": "../../../../chart.js/node_modules/moment/locale/sl.js",
	"./sq": "../../../../chart.js/node_modules/moment/locale/sq.js",
	"./sq.js": "../../../../chart.js/node_modules/moment/locale/sq.js",
	"./sr": "../../../../chart.js/node_modules/moment/locale/sr.js",
	"./sr-cyrl": "../../../../chart.js/node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../chart.js/node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../chart.js/node_modules/moment/locale/sr.js",
	"./ss": "../../../../chart.js/node_modules/moment/locale/ss.js",
	"./ss.js": "../../../../chart.js/node_modules/moment/locale/ss.js",
	"./sv": "../../../../chart.js/node_modules/moment/locale/sv.js",
	"./sv.js": "../../../../chart.js/node_modules/moment/locale/sv.js",
	"./sw": "../../../../chart.js/node_modules/moment/locale/sw.js",
	"./sw.js": "../../../../chart.js/node_modules/moment/locale/sw.js",
	"./ta": "../../../../chart.js/node_modules/moment/locale/ta.js",
	"./ta.js": "../../../../chart.js/node_modules/moment/locale/ta.js",
	"./te": "../../../../chart.js/node_modules/moment/locale/te.js",
	"./te.js": "../../../../chart.js/node_modules/moment/locale/te.js",
	"./tet": "../../../../chart.js/node_modules/moment/locale/tet.js",
	"./tet.js": "../../../../chart.js/node_modules/moment/locale/tet.js",
	"./th": "../../../../chart.js/node_modules/moment/locale/th.js",
	"./th.js": "../../../../chart.js/node_modules/moment/locale/th.js",
	"./tl-ph": "../../../../chart.js/node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../chart.js/node_modules/moment/locale/tl-ph.js",
	"./tlh": "../../../../chart.js/node_modules/moment/locale/tlh.js",
	"./tlh.js": "../../../../chart.js/node_modules/moment/locale/tlh.js",
	"./tr": "../../../../chart.js/node_modules/moment/locale/tr.js",
	"./tr.js": "../../../../chart.js/node_modules/moment/locale/tr.js",
	"./tzl": "../../../../chart.js/node_modules/moment/locale/tzl.js",
	"./tzl.js": "../../../../chart.js/node_modules/moment/locale/tzl.js",
	"./tzm": "../../../../chart.js/node_modules/moment/locale/tzm.js",
	"./tzm-latn": "../../../../chart.js/node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../chart.js/node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../chart.js/node_modules/moment/locale/tzm.js",
	"./uk": "../../../../chart.js/node_modules/moment/locale/uk.js",
	"./uk.js": "../../../../chart.js/node_modules/moment/locale/uk.js",
	"./ur": "../../../../chart.js/node_modules/moment/locale/ur.js",
	"./ur.js": "../../../../chart.js/node_modules/moment/locale/ur.js",
	"./uz": "../../../../chart.js/node_modules/moment/locale/uz.js",
	"./uz-latn": "../../../../chart.js/node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../chart.js/node_modules/moment/locale/uz-latn.js",
	"./uz.js": "../../../../chart.js/node_modules/moment/locale/uz.js",
	"./vi": "../../../../chart.js/node_modules/moment/locale/vi.js",
	"./vi.js": "../../../../chart.js/node_modules/moment/locale/vi.js",
	"./x-pseudo": "../../../../chart.js/node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../chart.js/node_modules/moment/locale/x-pseudo.js",
	"./yo": "../../../../chart.js/node_modules/moment/locale/yo.js",
	"./yo.js": "../../../../chart.js/node_modules/moment/locale/yo.js",
	"./zh-cn": "../../../../chart.js/node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../chart.js/node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "../../../../chart.js/node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../chart.js/node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "../../../../chart.js/node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../chart.js/node_modules/moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../chart.js/node_modules/moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map